# accphylib

Collection of utilities for cern acclerators. 

I have included many scripts taken from `SPSMeasurementTools` which was a project written by K. Li et al. 
Please refer to them for info. The code has been included only for simplicity and all the credits
are for the original authors. 
