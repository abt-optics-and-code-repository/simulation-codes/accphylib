"""
setup.py for accphylib.

For reference see
https://packaging.python.org/guides/distributing-packages-using-setuptools/

"""
from pathlib import Path
from setuptools import setup, find_packages
import os
import ast


HERE = Path(__file__).parent.absolute()
with (HERE / "README.md").open("rt") as fh:
    LONG_DESCRIPTION = fh.read().strip()

MODULE_NAME = "accphylib"


def read_version_from_init():
    """
    Extracts version from __init.py__. Inspired by PyJAPC.
    """
    init_file = os.path.join(os.path.dirname(__file__), MODULE_NAME, "__init__.py")
    with open(init_file, "r") as file_handle:
        for line in file_handle:
            if line.startswith("__version__"):
                return ast.literal_eval(line.split("=", 1)[1].strip())

    raise Exception("Failed to extract version from __init__.py")


REQUIREMENTS: dict = {
    "core": [
        "scipy>=1.5",
        "matplotlib>=3.3",
        "prettytable",
        "pandas>=1.1",
        "future",
        "scikit-learn",
    ],
    "full": [
        "pytimber==3.1.*",
    ],
    "test": [
        "pytest",
    ],
    "dev": [
        # 'requirement-for-development-purposes-only',
    ],
    "doc": [
        "sphinx",
        "acc-py-sphinx",
    ],
}


setup(
    name="accphylib",
    version=read_version_from_init(),
    author="Francesco Maria Velotti",
    author_email="francesco.maria.velotti@cern.ch",
    description="SHORT DESCRIPTION OF PROJECT",
    long_description=LONG_DESCRIPTION,
    long_description_content_type="text/markdown",
    url="",
    packages=find_packages(),
    python_requires=">=3.6, <4",
    classifiers=[
        "Programming Language :: Python :: 3",
        "Operating System :: OS Independent",
    ],
    install_requires=REQUIREMENTS["core"],
    extras_require={
        **REQUIREMENTS,
        # The 'dev' extra is the union of 'test' and 'doc', with an option
        # to have explicit development dependencies listed.
        "dev": [
            req
            for extra in ["dev", "test", "doc"]
            for req in REQUIREMENTS.get(extra, [])
        ],
        # The 'all' extra is the union of all requirements.
        "all": [req for reqs in REQUIREMENTS.values() for req in reqs],
    },
)
