from __future__ import print_function
import numpy as np

colors = [
    (0.0, 0.0, 0.0),
    (0.996, 0.3608, 0.0275),
    (0.996, 0.988, 0.0353),
    (0.541, 0.7137, 0.0275),
    (0.1451, 0.4353, 0.3843),
    (0.00784, 0.5098, 0.9294),
    (0.1529, 0.1137, 0.4902),
    (0.4706, 0.2627, 0.5843),
    (0.8902, 0.01176, 0.4902),
]


hex_col = []
for color in colors:
    color = tuple(np.array(color) * 255)
    hex_col.append("%02x%02x%02x" % color)

print(hex_col)
