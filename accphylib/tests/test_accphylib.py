"""
High-level tests for the  package.

"""

import accphylib


def test_version():
    assert accphylib.__version__ is not None
