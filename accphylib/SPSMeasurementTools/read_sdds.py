from __future__ import print_function
from __future__ import absolute_import
from .sddsdata import sddsdata


def sdds_to_dict(in_complete_path):
    try:
        temp = sddsdata(in_complete_path, endian="little", full=True)
    except IndexError:
        print("Failed to open data file.")
        return
    data = temp.data[0]

    filename = in_complete_path.split("/")[-1]
    # print 'filename', filename
    return data
