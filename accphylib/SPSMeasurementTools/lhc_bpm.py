from __future__ import division
from __future__ import print_function
from __future__ import absolute_import
from builtins import str
from builtins import object
from past.utils import old_div
from .sddsdata import sddsdata
import numpy as np
import scipy.io as sio
import os
import shutil
import gzip
import time
import glob
import pickle
from .zip_mat import save_zip
from . import timestamp_helpers as th


class LhcBpm(object):
    def __init__(self, dict_lhcbpm, bpm_name, bunch_num):
        # temp_filename = complete_path.split('.gz')[0] + 'uzipd'
        # with open(temp_filename, "wb") as tmp:
        #    shutil.copyfileobj(gzip.open(complete_path), tmp)
        # dict_qmeter = sio.loadmat(temp_filename)
        # os.remove(temp_filename)

        self.name = bpm_name
        variables = []
        for key in sorted(dict_lhcbpm):
            if bpm_name in key:
                variables.append(key)

        # print variables

        variables = [
            variables[bunch_num - 1],
            variables[old_div(len(variables), 2) + (bunch_num - 1)],
        ]

        print(variables)

        for variable in variables:
            if "H" in variable:
                self.datah = dict_lhcbpm[variable]
            else:
                self.datav = dict_lhcbpm[variable]

    def get_fft(self, plane="H"):
        """Use plane='H' for horizontal and plane='V' for vertical
        plane data."""

        if plane == "H":
            rawData = self.datah
        elif plane == "V":
            rawData = self.datav
        else:
            print("unknown plane argument")
            return -1

        n_turns = len(rawData)
        ampl = np.abs(np.fft.rfft(rawData))
        freq = np.fft.rfftfreq(n_turns)

        return freq, ampl


def sdds_to_dict(in_complete_path):
    try:
        temp = sddsdata(in_complete_path, endian="little", full=True)
    except IndexError:
        print("Failed to open data file.")
        return
    data = temp.data[0]

    filename = in_complete_path.split("/")[-1]
    print("filename", filename)

    HorBunchId = np.int_(data["HorBunchId"])
    MonNames = data["MonNames"]

    dict_meas = {"HorBunchId": HorBunchId, "MonNames": MonNames}

    for name in MonNames:
        for bunch in HorBunchId:
            if np.float_(data[name + "/H-bunch" + str(bunch)])[0] != 0.0:
                dict_meas[name + "/H-bunch" + str(bunch)] = data[
                    name + "/H-bunch" + str(bunch)
                ]
                dict_meas[name + "/V-bunch" + str(bunch)] = data[
                    name + "/V-bunch" + str(bunch)
                ]

    return dict_meas


def sdds_to_file(
    in_complete_path, mat_filename_prefix="SPSmeas_", outp_folder="qmeter/"
):

    dict_meas = sdds_to_dict(in_complete_path)
    us_string = dict_meas["SPSuser"]
    t_stamp_unix = dict_meas["t_stamp_unix"]
    device_name = dict_meas["deviceName"]

    out_filename = (
        mat_filename_prefix + us_string + "_" + device_name + ("_%d" % t_stamp_unix)
    )
    out_complete_path = outp_folder + us_string + "/" + out_filename

    if not os.path.isdir(outp_folder + us_string):
        print("I create folder: " + outp_folder + us_string)
        os.makedirs(outp_folder + us_string)

    sio.savemat(out_complete_path, dict_meas, oned_as="row")
    save_zip(out_complete_path)
