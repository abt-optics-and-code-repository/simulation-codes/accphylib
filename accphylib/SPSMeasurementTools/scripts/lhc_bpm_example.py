from __future__ import print_function
from SPSMeasurementTools import lhc_bpm
import os
import matplotlib.pyplot as plt

"""
How to use LHC-BPM
"""

path = "/afs/cern.ch/work/f/fvelotti/private/sps_op/detuning/"
files = os.listdir(path)
print(len(files))

# Make a dictionary for the sdds file
bpm_data = lhc_bpm.sdds_to_dict(path + files[0])

bpm_obj = lhc_bpm.LhcBpm(bpm_data, "SPS.BPMB.51905", 2)
plt.plot(bpm_obj.datah, "x")


# Make FFT from data
freq, ampl = bpm_obj.get_fft()

# Plot it
plt.figure(2)
plt.plot(freq, ampl)

plt.show()
