from __future__ import print_function
from SPSMeasurementTools import QMeter
import os
import matplotlib.pyplot as plt

"""
How to use QMeter
"""

path = "/afs/cern.ch/work/f/fvelotti/private/sps_op/detuning/"
files = os.listdir(path)
print(len(files))

# Make a dictionary for the sdds file
bbq = QMeter.sdds_to_dict(path + files[40])

print(len(bbq["rawDataH"]))
print(bbq["nbOfTurns"])

# Create an instance of QMeter from the above dictionary
bbq_obj = QMeter.QMeter(bbq)

# Make FFT from data
freq, ampl = bbq_obj.get_fft()

# Plot it
plt.plot(bbq["rawDataH"])
plt.show()
