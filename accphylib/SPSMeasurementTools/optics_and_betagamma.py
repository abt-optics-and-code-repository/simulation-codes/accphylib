from __future__ import division
from past.utils import old_div
from scipy.interpolate import interp1d
from scipy.constants import m_p, e, c
from numpy import array
import numpy as np
import csv

beta_opt_lib = {}

beta_opt_lib["SPS.BWS.41677.H_ROT:Q26"] = 38.03
beta_opt_lib["SPS.BWS.41677.V_ROT:Q26"] = 62.96

beta_opt_lib["SPS.BWS.41677.H_ROT:Q20"] = 49.19
beta_opt_lib["SPS.BWS.41677.V_ROT:Q20"] = 71.02

beta_opt_lib["SPS.BWS.51995.H_ROT:Q26"] = 81.49
beta_opt_lib["SPS.BWS.51995.V_ROT:Q26"] = 28.15

beta_opt_lib["SPS.BWS.51995.H_ROT:Q20"] = 86.8
beta_opt_lib["SPS.BWS.51995.V_ROT:Q20"] = 39.6


def retrieve_betagamma(cycle_csv_filepath, t_in_cycle_ms):
    list_values = []
    with open(cycle_csv_filepath) as csf:
        reader = csv.DictReader(csf)
        for row in reader:
            list_values.append(row)

    t = []
    momentum = []
    for v in list_values:
        if v["X"] == "" or v["Y"] == "":
            continue
        t.append(float(v["X"]))
        momentum.append(float(v["Y"]))
    t = np.array(t)
    momentum = np.array(momentum)

    momentum_interp = interp1d(t, momentum, kind="linear")
    m_p_GeV = old_div(m_p * c ** 2, (1e9 * e))
    return old_div(momentum_interp(t_in_cycle_ms), m_p_GeV)
