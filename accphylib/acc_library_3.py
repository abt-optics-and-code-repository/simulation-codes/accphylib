#!/usr/bin/python
import fileinput
import sys
import time
from scipy.optimize import curve_fit
import math
import numpy as np
import matplotlib.pyplot as plt
from scipy.stats import norm as gauss
import matplotlib as mpl
from scipy.stats.kde import gaussian_kde
import zipfile
import os.path
import re
from datetime import datetime
from datetime import timedelta
from datetime import date
import matplotlib.patches as patches
from prettytable import PrettyTable
import linecache
from scipy.stats import truncnorm as trandn
import random
import pickle
from scipy.interpolate import interp1d
from random import shuffle
from scipy.stats import norm, expon
import platform
from SPSMeasurementTools.sddsdata_3 import sddsdata
import pandas as pd
import pytimber
from sklearn.mixture import GaussianMixture
from matplotlib.colors import LogNorm
import itertools
import operator
from scipy.signal import argrelextrema
import scipy.io as spio
import csv
from mpl_toolkits.axes_grid1 import make_axes_locatable

from bokeh.plotting import figure as b_fig

colours = list(mpl.rcParams["axes.prop_cycle"])

fig_w_norm = 3.25
fig_h_norm = fig_w_norm * 0.68  # 10% more than goldenratio

fig_w_large = 7.0
fig_h_large = fig_w_large * 0.68 * 0.6

_lines = ["-", "--", "-.", ":"]
lines = itertools.cycle(_lines)

_markers = ["o", "s", "v", "^", "D", "<", ">", "p", "h"]
markers = itertools.cycle(_markers)

# for making hollow markers, use it together with marker_types
_markersh = ["o", "o", "s", "s", "v", "v", "^", "^", "D", "D"]
markersh = itertools.cycle(_markersh)

_marker_types = [False, True]  # True for hollow markers
marker_types = itertools.cycle(_marker_types)

mfcs = itertools.cycle(["w", None])


class Particle(object):
    def __init__(self, x, xp, y, yp, t, dpp, p0):
        self.x = x
        self.xp = xp
        self.y = y
        self.yp = yp
        self.t = t
        self.dpp = dpp
        self.p0 = p0
        self.cpx = self.xp * self.p0
        self.cpy = self.yp * self.p0


class ParticleBeam(object):
    def __init__(
        self,
        x0,
        xp0,
        y0,
        yp0,
        dpp0,
        delta,
        mom,
        emitnx,
        emitny,
        betx,
        bety,
        alfx,
        alfy,
        type="p",
    ):
        self.mom = mom
        self.x0 = x0
        self.xp0 = xp0
        self.y0 = y0
        self.yp0 = yp0
        self.dpp0 = dpp0  # Momentum offset
        self.delta = delta  # Momentum spread
        self.emitnx = emitnx
        self.emitny = emitny
        self.betx = betx
        self.bety = bety
        self.alfx = alfx
        self.alfy = alfy

        # ========================================
        # Beam parameters
        # ========================================

        if type == "p":
            p = mom  # momentum in GeV
            self.gamma = np.sqrt(0.938 ** 2 + p ** 2) / 0.938
            self.beta_r = np.sqrt(1.0 - 1.0 / self.gamma ** 2)
            self.geo_emit_x = self.emitnx / (self.beta_r * self.gamma)
            self.geo_emit_y = self.emitny / (self.beta_r * self.gamma)

        elif type == "ion":

            self.gamma = mom * 82.0 / 193.7  # Approx tot E with p
            self.beta_r = np.sqrt(1.0 - 1.0 / self.gamma ** 2)
            self.geo_emit_x = self.emitnx / (self.beta_r * self.gamma)
            self.geo_emit_y = self.emitny / (self.beta_r * self.gamma)

        self.x_size = np.sqrt(self.geo_emit_x * self.betx)
        self.y_size = np.sqrt(self.geo_emit_y * self.bety)

        self.vec_x = np.array([x0])
        self.vec_y = np.array([y0])
        self.vec_xp = np.array([xp0])
        self.vec_yp = np.array([yp0])
        self.vec_t = 0
        self.vec_dpp = np.array([dpp0])

        self.xn = 0
        self.xpn = 0
        self.jx = 0
        self.phix = 0
        self.yn = 0
        self.ypn = 0
        self.jy = 0
        self.phiy = 0

        self.mean_in_time = []
        self.mean_in_time_xp = []

    @property
    def x(self):
        return self._x

    @property
    def vec_x(self):
        return self._vec_x

    @x.setter
    def x(self, value):
        self._x = value
        self._vec_x += value

    @vec_x.setter
    def vec_x(self, value):
        self._vec_x = value
        self._x = np.mean(self._vec_x)

    @property
    def xp(self):
        return self._xp

    @property
    def vec_xp(self):
        return self._vec_xp

    @xp.setter
    def xp(self, value):
        self._xp = value
        self._vec_xp += value

    @vec_xp.setter
    def vec_xp(self, value):
        self._vec_xp = value
        self._xp = np.mean(self._vec_xp)

    @property
    def y(self):
        return self._y

    @property
    def vec_y(self):
        return self._vec_y

    @y.setter
    def y(self, value):
        self._y = value
        self._vec_y += value

    @vec_y.setter
    def vec_y(self, value):
        self._vec_y = value
        self._y = np.mean(self._vec_y)

    @property
    def yp(self):
        return self._yp

    @property
    def vec_yp(self):
        return self._vec_yp

    @yp.setter
    def yp(self, value):
        self._yp = value
        self._vec_yp += value

    @vec_yp.setter
    def vec_yp(self, value):
        self._vec_yp = value
        self._yp = np.mean(self._vec_yp)

    def removeParticles(self, bool_vec):
        self.vec_x = self.vec_x[bool_vec]
        self.vec_y = self.vec_y[bool_vec]
        self.vec_xp = self.vec_xp[bool_vec]
        self.vec_yp = self.vec_yp[bool_vec]
        self.vec_t = self.vec_t[bool_vec]
        self.vec_dpp = self.vec_dpp[bool_vec]

    def get_norm_x(self):
        self.xn = self.vec_x / np.sqrt(self.betx)
        self.xpn = (self.vec_x * self.alfx + self.vec_xp * self.betx) / np.sqrt(
            self.betx
        )

    def get_jx_phix(self):
        self.get_norm_x()
        self.jx = 0.5 * (self.xn ** 2 + self.xpn ** 2)
        self.phix = np.arctan2(self.xpn, self.xn)

    def get_norm_y(self):
        self.yn = self.vec_y / np.sqrt(self.bety)
        self.ypn = (self.vec_y * self.alfy + self.vec_yp * self.bety) / np.sqrt(
            self.bety
        )

    def get_jy_phiy(self):
        self.get_norm_y()
        self.jy = 0.5 * (self.yn ** 2 + self.ypn ** 2)
        self.phiy = np.arctan2(self.ypn, self.yn)

    def update_from_action(self):
        self.xn = np.sqrt(2 * self.jx) * np.cos(self.phix)
        self.xpn = np.sqrt(2 * self.jx) * np.sin(self.phix)

        self.yn = np.sqrt(2 * self.jy) * np.cos(self.phiy)
        self.ypn = np.sqrt(2 * self.jy) * np.sin(self.phiy)

        self.vec_x = np.sqrt(2 * self.jx * self.betx) * np.cos(self.phix)
        self.vec_y = np.sqrt(2 * self.jy * self.bety) * np.cos(self.phiy)

        self.vec_xp = (
            np.sqrt(self.betx) * self.xpn - self.alfx * self.vec_x
        ) / self.betx
        self.vec_yp = (
            np.sqrt(self.bety) * self.ypn - self.alfy * self.vec_y
        ) / self.bety

    def make_particle_distribution(
        self,
        n=1000,
        plane="hv",
        seed=123,
        dx=0,
        dpx=0,
        dy=0,
        dpy=0,
        sigmacut=5,
        dpp_distr="Norm",
    ):
        betx = self.betx
        bety = self.bety
        alfx = self.alfx
        alfy = self.alfy

        # ========================================
        # Seed
        # ========================================

        np.random.seed(seed)
        n_sigma = sigmacut
        m_p = 0.938

        # ========================================
        # Beam parameters
        # ========================================

        p = self.mom  # momentum in GeV

        gamma = np.sqrt(m_p ** 2 + p ** 2) / m_p
        beta0 = np.sqrt(gamma ** 2 - 1) / gamma
        beta_r = np.sqrt(1.0 - 1.0 / gamma ** 2)
        emit_x = self.emitnx / (beta_r * gamma)
        emit_y = self.emitny / (beta_r * gamma)

        e0 = np.sqrt(p ** 2 + m_p ** 2)

        # ========================================
        # Delta p / p
        # ========================================

        if dpp_distr == "Uni":
            dpp = ((np.random.rand(n) * 2.0 * self.delta) - self.delta) + self.dpp0
        elif dpp_distr == "Norm":
            dpp = trandn(-3, 3, scale=self.delta).rvs(n) + self.dpp0

        self.de = dpp * e0 * beta0 ** 2
        self.pt = self.de / p

        # ==============================================
        # Bivariate normal distributions
        # ==============================================

        sx = np.sqrt(emit_x * betx)
        n_x = trandn(-1 * n_sigma, n_sigma, scale=sx).rvs(n)
        x = self.x0 + n_x + (dx * dpp)
        xp = (
            self.xp0
            + (trandn(-1 * n_sigma, n_sigma, scale=sx).rvs(n) - alfx * n_x) / betx
            + (dpx * dpp)
        )

        sy = np.sqrt(emit_y * bety)
        n_y = trandn(-1 * n_sigma, n_sigma, scale=sy).rvs(n)
        y = self.y0 + n_y + (dy * dpp)
        yp = (
            self.yp0
            + (trandn(-1 * n_sigma, n_sigma, scale=sy).rvs(n) - alfy * n_y) / bety
            + (dpy * dpp)
        )

        t = np.zeros(n)

        self.vec_x = x
        self.vec_y = y
        self.vec_xp = xp
        self.vec_yp = yp
        self.vec_t = t
        self.vec_dpp = dpp

        self.x = np.mean(x)
        self.y = np.mean(y)
        self.xp = np.mean(xp)
        self.yp = np.mean(yp)
        self.dpp = np.mean(dpp)

    def track_oneturn_action_angle(self, delta_mux, delta_muy):
        self.phix = self.phix - delta_mux * 2 * np.pi
        self.phiy = self.phiy - delta_muy * 2 * np.pi

        self.update_from_action()

        self.mean_in_time.append(self.x)
        self.mean_in_time_xp.append(self.xp)

    def h_kick(self, val):
        self.vec_xp += val
        self.get_jx_phix()

    def track_one_turn(self, tune_x, tune_y):
        A, B, C, D = one_turn_map(self.betx, self.alfx, tune_x)

        x_new = self.vec_x * A + self.vec_xp * B
        xp_new = self.vec_x * C + self.vec_xp * D

        self.vec_x = x_new
        self.vec_xp = xp_new


def density_plot(x, y, eff_p, x_plus, y_plus):
    """
    This function cab be used to make a density plot in particle/mm^2

    :param x: array of particle's horizontal coordinate
    :param y: array of particle's vertical coordinate
    :param eff_p: intensity / particles_tracked
    :param x_plus: of how many mm one wants to increase the histogram area (horizontal)
    :param y_plus: of how many mm one wants to increase the histogram area (vertical)
    """

    ax = min(x * 1e3)
    bx = max(x * 1e3)
    ay = min(y * 1e3)
    by = max(y * 1e3)

    binx = math.ceil(bx - ax) + x_plus
    biny = math.ceil(by - ay) + y_plus
    restx = (binx - (bx - ax)) / 2
    resty = (biny - (by - ay)) / 2
    binx_min = math.floor(ax - restx)
    binx_max = math.ceil(bx + restx)

    biny_min = math.floor(ay - resty)
    biny_max = math.ceil(by + resty)
    binx = binx_max - binx_min
    biny = biny_max - biny_min

    bin_range1 = [[binx_min, binx_max], [biny_min, biny_max]]

    H1, xedges1, yedges1 = np.histogram2d(
        x * 1e3, y * 1e3, range=bin_range1, bins=(binx, biny), normed=False
    )
    print("Max p/mm2 = %.4e" % (H1.max() * eff_p))
    extent1 = [xedges1[0], xedges1[-1], yedges1[0], yedges1[-1]]
    # plt.hist2d(x, y, bins=(29, 25), range=bin_range, normed=False)

    cmap = plt.get_cmap("viridis")
    # cmap = plt.get_cmap('PuBu_r')

    plt.imshow(
        H1.transpose() * eff_p,
        origin="lower",
        aspect="auto",
        cmap=cmap,
        extent=extent1,
        norm=LogNorm(),
    )
    plt.gca().invert_xaxis()

    # 'gist_stern'
    plt.xlabel("x / m")
    plt.ylabel("y / m")
    cb = plt.colorbar()
    cb.set_label("p+ / mm$^2$")
    # plt.tight_layout()
    return H1.max() * eff_p


def kde_density_estimation(m1, m2):
    xmin = min(m1) * 0.5
    xmax = max(m1) * 1.5
    ymin = min(m2) * 0.5
    ymax = max(m2) * 1.5

    X, Y = np.mgrid[xmin:xmax:100j, ymin:ymax:100j]
    positions = np.vstack([X.ravel(), Y.ravel()])
    values = np.vstack([m1, m2])
    kernel = gaussian_kde(values)
    Z = np.reshape(kernel(positions).T, X.shape)

    return X, Y, Z


def plot_kde_density(m1, m2, colorbar=False, labels=False, numLevels=10):
    X, Y, Z = kde_density_estimation(m1, m2)

    # Show density
    levs = np.logspace(
        np.log10(min(Z.flatten()) + 1e-20), np.log10(max(Z.flatten())), num=numLevels
    )

    plt.contourf(X, Y, Z, norm=LogNorm(), levels=levs, alpha=0.5)

    if colorbar:
        plt.colorbar()

    plt.plot(m1, m2, "k.", alpha=0.5)
    cs = plt.contour(
        X, Y, Z, norm=LogNorm(), levels=levs[::2], colors="k", linewidths=0.2
    )

    if labels:
        plt.clabel(cs, inline=1, fontsize=10)


def density_plot_binrange(x, y, eff_p, x_plus, y_plus, ax, bx, ay, by):
    """
    This function cab be used to make a density plot in particle/mm^2

    :param x: array of particle's horizontal coordinate
    :param y: array of particle's vertical coordinate
    :param eff_p: intensity / particles_tracked
    :param x_plus: of how many mm one wants to increase the histogram area (horizontal)
    :param y_plus: of how many mm one wants to increase the histogram area (vertical)
    """

    # ax = min(x * 1e3)
    # bx = max(x * 1e3)
    # ay = min(y * 1e3)
    # by = max(y * 1e3)

    binx = math.ceil(bx - ax)
    biny = math.ceil(by - ay)
    restx = (binx - (bx - ax)) / 2
    resty = (biny - (by - ay)) / 2
    binx_min = math.floor(ax - restx)
    binx_max = math.ceil(bx + restx)

    biny_min = math.floor(ay - resty)
    biny_max = math.ceil(by + resty)
    binx = binx_max - binx_min
    biny = biny_max - biny_min

    bin_range1 = [
        [binx_min * 1e-3, binx_max * 1e-3],
        [biny_min * 1e-3, biny_max * 1e-3],
    ]

    H1, xedges1, yedges1 = np.histogram2d(
        x, y, range=bin_range1, bins=(binx, biny), normed=False
    )
    print("Max p/mm2 = %.2e" % (H1.max() * eff_p))
    extent1 = [
        xedges1[0] - x_plus,
        xedges1[-1] + x_plus,
        yedges1[0] - y_plus,
        yedges1[-1] + y_plus,
    ]
    # plt.hist2d(x, y, bins=(29, 25), range=bin_range, normed=False)

    cmap = plt.get_cmap("PuBu_r")

    plt.imshow(
        H1.transpose() * eff_p,
        origin="lower",
        cmap=cmap,
        extent=extent1,
        interpolation="quadric",
    )
    plt.gca().invert_xaxis()

    # 'gist_stern'
    plt.xlabel("x (m)")
    plt.ylabel("y (m)")
    cb = plt.colorbar()
    cb.set_label("p+ / mm$^2$")
    # plt.tight_layout()


def contour_plot(x, y, eff_p, x_plus, y_plus):
    """
    This function cab be used to make a density plot in particle/mm^2

    :param x: array of particle's horizontal coordinate
    :param y: array of particle's vertical coordinate
    :param eff_p: intensity / particles_tracked
    :param x_plus: of how many mm one wants to increase the histogram area (horizontal)
    :param y_plus: of how many mm one wants to increase the histogram area (vertical)
    """

    ax = min(x * 1e3)
    bx = max(x * 1e3)
    ay = min(y * 1e3)
    by = max(y * 1e3)

    binx = math.ceil(bx - ax) + x_plus
    biny = math.ceil(by - ay) + y_plus
    restx = (binx - (bx - ax)) / 2
    resty = (biny - (by - ay)) / 2
    binx_min = math.floor(ax - restx)
    binx_max = math.ceil(bx + restx)

    biny_min = math.floor(ay - resty)
    biny_max = math.ceil(by + resty)
    binx = binx_max - binx_min
    biny = biny_max - biny_min

    bin_range1 = [
        [binx_min * 1e-3, binx_max * 1e-3],
        [biny_min * 1e-3, biny_max * 1e-3],
    ]

    H1, xedges1, yedges1 = np.histogram2d(
        x, y, range=bin_range1, bins=(binx, biny), normed=False
    )
    print("Max p/mm2 = %.2e" % (H1.max() * eff_p))
    extent1 = [xedges1[0], xedges1[-1], yedges1[0], yedges1[-1]]
    # plt.hist2d(x, y, bins=(29, 25), range=bin_range, normed=False)

    plt.imshow(
        H1.transpose() * eff_p,
        origin="lower",
        cmap="hot",
        extent=extent1,
        interpolation="spline36",
    )
    cb = plt.colorbar()
    cb.set_label("p+ / mm$^2$")
    # plt.gca().invert_xaxis()
    levels = (2.0e11, 1.0e11, 5.0e10, 1.0e10)
    cs = plt.contour(H1.transpose() * eff_p, extent=extent1)
    plt.clabel(cs, outline=1, fontsize=12, fmt="%1.e")
    plt.gca().invert_xaxis()
    # 'gist_stern'
    plt.xlabel("x (m)")
    plt.ylabel("y (m)")

    # plt.tight_layout()


def density_max(x, y, eff_p):
    """
    This function cab be used to make a density plot in particle/mm^2

    :param x: array of particle's horizontal coordinate
    :param y: array of particle's vertical coordinate
    :param eff_p: number of real particles corresponding to each macro-particle
    """

    ax = min(x * 1e3)
    bx = max(x * 1e3)
    ay = min(y * 1e3)
    by = max(y * 1e3)

    binx = math.ceil(bx - ax)
    biny = math.ceil(by - ay)
    restx = (binx - (bx - ax)) / 2
    resty = (biny - (by - ay)) / 2
    binx_min = math.floor(ax - restx)
    binx_max = math.ceil(bx + restx)

    biny_min = math.floor(ay - resty)
    biny_max = math.ceil(by + resty)
    binx = binx_max - binx_min
    biny = biny_max - biny_min

    bin_range1 = [
        [binx_min * 1e-3, binx_max * 1e-3],
        [biny_min * 1e-3, biny_max * 1e-3],
    ]

    H1, xedges1, yedges1 = np.histogram2d(
        x, y, range=bin_range1, bins=(binx, biny), normed=False
    )
    print(
        "{0:.2e}".format(H1.max() * eff_p),
        "tot p+/element: ",
        "{0:.2e}".format(len(x) * eff_p),
    )
    return H1.max() * eff_p


def loss_hist(s, s2, circ, eff_p, pic_title):
    """
    Creates an histogram with losses around the whole SPS

    :param s2: array with the longitudinal coordinate where the particle is lost in the second turn
    :param pic_title: title of the figure
    :param s: array with the longitudinal coordinate where the particle is lost
    :param circ: length of the machine to considered
    :param eff_p:  number of real particles corresponding to each macro-particle
    """
    plt.figure(figsize=(12, 2.5))
    bin_range_loss = (0, circ)
    hist, bins = np.histogram(s, bins=circ, range=bin_range_loss)
    hist2, bins2 = np.histogram(s2, bins=circ, range=bin_range_loss)

    plt.plot(hist * eff_p, lw=2.5, c="darkcyan")
    plt.plot(hist2 * eff_p, c="orangered", lw=2.5)
    plt.xlabel("s (m)")
    plt.ylabel("Losses")
    plt.legend(["1st turn", "2nd turn"], loc=9)
    plt.yscale("symlog")
    plt.ylim(1e8, 1e14)
    plt.xlim(0, circ)
    plt.title(pic_title)
    # plt.minorticks_on()


def loss_hist_oneturn(s, circ, eff_p, col, width):
    """
    Creates an histogram with losses around the whole SPS

    :param col: plot color
    :param width: line width
    :param s: array with the longitudinal coordinate where the particle is lost
    :param circ: length of the machine to considered
    :param eff_p:  number of real particles corresponding to each macro-particle
    """
    # plt.figure(1, figsize=(12, 2.5), facecolor='w')
    bin_range_loss = (1, circ)
    hist, bins = np.histogram(s, bins=int(circ), range=bin_range_loss)

    plt.plot(hist * eff_p, lw=width, c=col)

    # plt.legend(["1st turn", "2nd turn"], loc=9)
    # plt.yscale('symlog')
    # plt.ylim(1e5, 1E14)
    plt.xlim(0, circ)
    # plt.tight_layout()


def get_twiss_var(file_name, path, list_var):
    """
    Gets a twiss and a list of twiss columns as input and gives a matrix where each
    column represents a twiss column in the given order

    @param file_name: twiss file name
    @param path: path of the file
    @param list_var: list of strings of the required twiss columns
    @return: matrix of the asked twiss columns
    """
    list_var = [temp.upper() for temp in list_var]

    col_var = []
    variable = []

    with open(path + file_name, "r") as ff:
        for _ in range(1, 46):
            next(ff)

        names = next(ff).split()

        for nn in list_var:
            if nn in names:
                col_var.append(names.index(nn) - 1)
            else:
                print("Var %s not found!" % nn)
        for _ in range(len(col_var)):
            variable.append([])
        next(ff)

        if "KEYWORD" in list_var:
            i_kw = list_var.index("KEYWORD")
        else:
            i_kw = -1

        if "NAME" in list_var:
            i_n = list_var.index("NAME")
        else:
            i_n = -1

        for line in ff:
            col = line.split()
            for index in zip(col_var, range(0, len(col_var))):
                if index[1] == i_kw or index[1] == i_n:
                    variable[index[1]].append(col[index[0]])
                else:
                    variable[index[1]].append(float(col[index[0]]))

                    # if not list_var[0] == 'NAME':
                    #     for line in ff:
                    #         col = line.split()
                    #         for index in zip(col_var, xrange(0, len(col_var))):
                    #             variable[index[1]].append(float(col[index[0]]))
                    # else:
                    #      for line in ff:
                    #         col = line.split()
                    #         variable[0].append(col[0])
                    #         for index in zip(col_var[1:], xrange(1, len(col_var))):
                    #             variable[index[1]].append(float(col[index[0]]))
    return variable


def get_twiss_numpy(file_name, path, list_var):
    """
    Gets a twiss and a list of twiss columns as input and gives a matrix where each
    column represents a twiss column in the given order

    @param file_name: twiss file name
    @param path: path of the file
    @param list_var: list of strings of the required twiss columns
    @return: matrix of the asked twiss columns
    """
    list_var = [temp.upper() for temp in list_var]

    col_var = []
    variable = []

    with open(path + file_name, "r") as ff:
        for _ in range(1, 46):
            next(ff)

        names = next(ff).split()

        for nn in list_var:
            if nn in names:
                col_var.append(names.index(nn) - 1)
            else:
                print("Var %s not found!" % nn)
        for _ in range(len(col_var)):
            variable.append([])
        next(ff)
        if not (list_var[0] == "NAME"):
            for line in ff:
                col = line.split()
                for index in zip(col_var, range(0, len(col_var))):
                    variable[index[1]].append(float(col[index[0]]))
        else:
            for line in ff:
                col = line.split()
                variable[0].append(col[0])
                for index in zip(col_var[1:], range(1, len(col_var))):
                    variable[index[1]].append(float(col[index[0]]))
    return variable


def one_turn_map(betx, alfx, tune):
    A = np.cos(tune * 2 * np.pi) + alfx * np.sin(tune * 2 * np.pi)
    B = betx * np.sin(tune * 2 * np.pi)
    C = (-1 - alfx ** 2) / betx * np.sin(tune * 2 * np.pi)
    D = np.cos(tune * 2 * np.pi) - alfx * np.sin(tune * 2 * np.pi)

    return A, B, C, D


def getRelativisticInfo(p):
    m = 0.93827231  # p+ mass in GeV/c^2
    e_tot = np.sqrt(p ** 2 + m ** 2)
    gamma = np.sqrt(p ** 2 + m ** 2) / m
    beta = np.sqrt(1 - 1 / gamma ** 2)
    p = 1.67e-27 * beta * gamma * 3e8
    brho = p / 1.6e-19
    return gamma, beta, brho


def transp_matrix(bet1, bet2, alf1, alf2, mu1, mu2):
    dmu = (mu2 - mu1) * 2 * np.pi
    print("delta mu = ", dmu)

    A = np.sqrt(bet2 / bet1) * (np.cos(dmu) + alf1 * np.sin(dmu))

    B = np.sqrt(bet1 * bet2) * np.sin(dmu)

    C = ((-1 - alf1 * alf2) / np.sqrt(bet1 * bet2)) * np.sin(dmu) + (
        (alf1 - alf2) / np.sqrt(bet1 * bet2)
    ) * np.cos(dmu)

    D = np.sqrt(bet1 / bet2) * (np.cos(dmu) - alf2 * np.sin(dmu))

    return A, B, C, D


def transp_matrix_drift(l=1):
    A = 1

    B = l

    C = 0

    D = 1

    return A, B, C, D


def get_beam(beam_type):
    if beam_type == "LHC":
        beam = dict(
            type="LHC",
            momentum=450,
            emit_nom=3.5e-6,
            emit_n=3.5e-6,
            intensity=288 * 1.15e11,
            dpp_t=3e-4,
            n_particles=1e6,
            n_sigma=5,
        )
    elif beam_type == "LIU":
        beam = dict(
            type="LIU",
            momentum=450,
            emit_nom=3.5e-6,
            emit_n=1.37e-6,
            intensity=288 * 2.0e11,
            dpp_t=3e-4,
            n_particles=1e6,
            n_sigma=5,
        )
    elif beam_type == "LHC-meas":
        beam = dict(
            type="LHC-meas",
            momentum=450,
            emit_nom=3.5e-6,
            emit_n=2.0e-6,
            intensity=288 * 1.2e11,
            dpp_t=3e-4,
            n_particles=1e6,
            n_sigma=6.8,
        )
    else:
        beam = dict(
            type="HL-LHC",
            momentum=450,
            emit_nom=3.5e-6,
            emit_n=2.08e-6,
            intensity=288 * 2.32e11,
            dpp_t=3e-4,
            n_particles=1e6,
            n_sigma=5,
        )
    return beam


def brho_t(t):
    m = 0.93827231  # p+ mass in GeV/c^2
    e_tot = m + t  # [GeV]
    p_gev = np.sqrt(e_tot ** 2 - m ** 2)

    gamma = np.sqrt(p_gev ** 2 + m ** 2) / m
    beta = np.sqrt(1 - 1 / gamma ** 2)
    p = 1.67e-27 * beta * gamma * 3e8
    print("beta = ", beta)
    print("gamma = ", gamma)
    print("beta * gamma: ", beta * gamma)
    return p / 1.6e-19


def brho_e(p):
    m = 0.51099906e-3  # p+ mass in GeV/c^2
    e_tot = np.sqrt(p ** 2 + m ** 2)
    # print "Etot = ", e_tot
    gamma = np.sqrt(p ** 2 + m ** 2) / m
    beta = np.sqrt(1 - 1 / gamma ** 2)
    p = 9.11e-31 * beta * gamma * 3e8
    print("brho = ", p / 1.6e-19)
    return p / 1.6e-19


class Beam(object):
    def __init__(self, beam_type):
        bb = get_beam(beam_type)
        self.emit_n = bb["emit_n"]
        self.type = bb["type"]
        self.momentum = bb["momentum"]
        self.emit_nom = bb["emit_nom"]
        self.intensity = bb["intensity"]
        self.dpp = bb["dpp_t"]
        self.mass = 0.93827231
        self.e_tot = np.sqrt(self.momentum ** 2 + self.mass ** 2)
        self.gamma = self.e_tot / self.mass
        self.beta = np.sqrt(1 - 1 / self.gamma ** 2)

        def get_brho(self):
            p = 1.67e-27 * self.beta * self.gamma * 3e8
            return p / 1.6e-19


def hist_cool(
    data,
    bins=10,
    range=None,
    color="black",
    linew=2.5,
    density=True,
    fitted=False,
    color_fit="red",
    norm=1.0,
    label=None,
):
    histo, bin_edge = np.histogram(data, bins=bins, range=range, density=density)
    bins_centre = []
    for k in range(1, len(bin_edge)):
        bins_centre.append((bin_edge[k] + bin_edge[k - 1]) / 2.0)
    count = 0
    for i in range(0, len(histo)):
        if histo[i] > 0:
            count += 1
            if count == 1:
                plt.plot(
                    [bins_centre[i], bins_centre[i]],
                    [1e-10, histo[i] / float(norm)],
                    lw=linew,
                    color=color,
                    label=label,
                )
            plt.plot(
                [bins_centre[i], bins_centre[i]],
                [1e-10, histo[i] / float(norm)],
                lw=linew,
                color=color,
            )

    if fitted:
        mu = np.mean(data)
        sigma = np.std(data)
        pdf_x_x = np.linspace(-5 * sigma, 5 * sigma, 1000)
        pdf_x = gauss.pdf(pdf_x_x, loc=mu, scale=sigma)
        plt.plot(pdf_x_x, pdf_x, lw=1, color=color_fit)

    return histo / float(norm), bins_centre


def convertBinVec(bin_edge):
    bins_centre = []
    for k in range(1, len(bin_edge)):
        bins_centre.append((bin_edge[k] + bin_edge[k - 1]) / 2.0)
    return bins_centre


def get_hist_vec(data, bins=10, range_data=None, density=True, **kwargs):
    histo, bin_edge = np.histogram(
        data, bins=bins, range=range_data, density=density, **kwargs
    )
    bins_centre = []
    for k in range(1, len(bin_edge)):
        bins_centre.append((bin_edge[k] + bin_edge[k - 1]) / 2.0)
    return histo, bins_centre


def get_hist_vec_cum(data, bins=10, range=None, density=True, cumulative=True):
    histo, bin_edge, _ = plt.hist(
        data, bins=bins, range=range, density=density, cumulative=cumulative
    )
    plt.clf()
    bins_centre = []
    for k in range(1, len(bin_edge)):
        bins_centre.append((bin_edge[k] + bin_edge[k - 1]) / 2.0)
    return histo, bins_centre


def vline_plot(data_x, data_y, color="black", width=2.5, label=""):
    for i in range(0, len(data_y)):
        if i == 0:
            plt.plot(
                [data_x[i], data_x[i]],
                [0, data_y[i]],
                lw=width,
                color=color,
                label=label,
            )
        else:
            plt.plot([data_x[i], data_x[i]], [0, data_y[i]], lw=width, color=color)


def kde_plot(data, **kwargs):
    mu = np.mean(data)
    sigma = np.std(data)
    x_fit = np.linspace(mu - 4 * sigma, mu + 4 * sigma, 1000)
    pdf = gaussian_kde(data)

    p0 = plt.plot(x_fit, pdf(x_fit), lw=1.0, **kwargs)
    plt.fill(x_fit, pdf(x_fit), alpha=0.2, color=p0[0].get_color())


def smooth_hist(data, color="", ax=None, orientation="Vertical", **kwargs):

    x_fit = np.linspace(min(data), max(data), 1000)
    pdf = gaussian_kde(data)

    if orientation == "Vertical":
        x, y = x_fit, pdf(x_fit)
    else:
        y, x = x_fit, pdf(x_fit)

    if not ax == None:
        if color == "":
            p0 = ax.plot(x, y, **kwargs)

            ax.fill(x, y, color=p0[0].get_color(), alpha=0.2)
        else:
            ax.plot(x, y, c=color, **kwargs)

            ax.fill(x, y, color=color, alpha=0.2)
    else:
        if color == "":
            p0 = plt.plot(x, y, **kwargs)

            plt.fill(x, y, color=p0[0].get_color(), alpha=0.2)
        else:
            plt.plot(x, y, c=color, **kwargs)

            plt.fill(x, y, color=color, alpha=0.2)


def plotHist(
    x, y, figsize=(4, 3), histSize=0.2, plot_prop=[], plot_opt={}, hist_opt={}
):

    fig, axScatter = plt.subplots(figsize=figsize)
    plt.plot(x, y, *plot_prop, **plot_opt)

    divider = make_axes_locatable(axScatter)
    axHisty = divider.append_axes("right", histSize, pad=0.01, sharey=axScatter)

    # make some labels invisible

    axHisty.yaxis.set_tick_params(labelleft=False)

    smooth_hist(y, ax=axHisty, orientation="horizontal", **hist_opt)
    axHisty.axis("off")
    return axScatter


def hist2d(data1, data2, figsize=(4, 3), hist2d_opt={}, color1d="k", clabel=""):

    fig, axScatter = plt.subplots(figsize=figsize)
    plt.hist2d(data1, data2, **hist2d_opt)
    plt.colorbar(label=clabel)

    divider = make_axes_locatable(axScatter)
    axHistx = divider.append_axes("top", 0.2, pad=0.01, sharex=axScatter)
    axHisty = divider.append_axes("right", 0.2, pad=0.01, sharey=axScatter)

    # make some labels invisible
    axHistx.xaxis.set_tick_params(labelbottom=False)
    axHisty.yaxis.set_tick_params(labelleft=False)

    smooth_hist(data1, color=color1d, ax=axHistx)
    axHistx.axis("off")
    smooth_hist(data2, color=color1d, ax=axHisty, orientation="horizontal")
    axHisty.axis("off")
    return axScatter


def kde_vectors(data):

    x_fit = np.linspace(min(data), max(data), 1000)
    pdf = gaussian_kde(data)

    return x_fit, pdf(x_fit)


# Unzip file


def unzip(source_filename, dest_dir):
    with zipfile.ZipFile(source_filename) as zf:
        for member in zf.infolist():
            words = member.filename.split("/")
            path = dest_dir
            for word in words[:-1]:
                drive, word = os.path.splitdrive(word)
                head, word = os.path.split(word)
                if word in (os.curdir, os.pardir, ""):
                    continue
                path = os.path.join(path, word)
            zf.extract(member, path)


def search_next(i, list):
    for j in range(i, len(list)):
        if list[j] != 0:
            return list[j]


def get_apertures(aper_raw):
    new_aper = list(aper_raw)
    for i in range(len(new_aper) - 1, 0, -1):
        if new_aper[i] != 0:
            new_aper[-1] = new_aper[i]
            break

    for i in range(len(new_aper)):

        if new_aper[i] == 0:
            new_aper[i] = search_next(i, new_aper)

    return np.array(new_aper)


def corr(x, y):
    a = x - np.mean(x)
    b = y - np.mean(y)
    return np.sum(a * b) / np.sqrt(np.sum(a ** 2) * np.sum(b ** 2))


def get_bpm_data(name, y, name_list):
    y_bpm = []
    index = []
    # name_bpm = []
    j_start = 0
    for i in range(len(name_list)):
        for j in range(j_start, len(name)):
            if re.search(name_list[i][4:], name[j]) and re.search("BP", name[j]):
                index.append(j)
                j_start = j
                break

    for i in index:
        y_bpm.append(y[i])
        # name_bpm.append(name[i])

    y_bpm = np.array(y_bpm)

    return y_bpm


def get_timber_dates(date_ele, time_ele):
    date_ele = [
        data_1 + "_" + time_1[:-4] for data_1, time_1 in zip(date_ele, time_ele)
    ]
    dates = [datetime.strptime(date_1, "%Y-%m-%d_%H:%M:%S") for date_1 in date_ele]
    return dates


def get_names_data(name, y, name_list):
    """
    Get values of y at the elements in name_list. Here name is list with all accelerator elements
    @param name: list of all accelerator elements
    @param y: list of values at each accelerator elements
    @param name_list: list of elements of interest
    @return:
    """

    y_bpm = []
    index = []
    # name_bpm = []
    j_start = 0
    for i in range(len(name_list)):
        for j in range(j_start, len(name)):
            if re.search(name_list[i], name[j]):
                index.append(j)
                j_start = j
                break

    for i in index:
        y_bpm.append(y[i])
        # name_bpm.append(name[i])

    y_bpm = np.array(y_bpm)

    return y_bpm


def plot_line_seq(name, s, l, pos_y, heigth):
    """

    @param twiss: twiss file path
    @param pos_y: where to plot the beamline schematic view
    @param heigth: dimension of the sketch
    """

    for i in range(1, len(s)):
        x = s[i] - l[i]
        y = pos_y - heigth / 2.0
        xy = x, y
        xy_qd = x, pos_y - heigth
        xy_qf = x, y + heigth / 2.0
        if (
            re.search(r"\"QF", name[i])
            or re.search(r"\"MQ", name[i])
            or re.search(r"\"Q", name[i])
        ) and "F" in name[i]:
            p = patches.Rectangle(
                xy_qf,
                l[i],
                heigth,
                fill=True,
                edgecolor="k",
                facecolor="w",
                lw=1,
                alpha=1,
                clip_on=False,
            )
            plt.gca().add_patch(p)
        if (
            re.search(r"\"QD", name[i])
            or re.search(r"\"MQ", name[i])
            or re.search(r"\"Q", name[i])
        ) and re.search(r"D", name[i]):
            p = patches.Rectangle(
                xy_qd,
                l[i],
                heigth,
                fill=True,
                edgecolor="black",
                facecolor="w",
                lw=1,
                alpha=1,
                clip_on=False,
            )
            plt.gca().add_patch(p)
        if (
            re.search(r"\"TPS", name[i])
            or re.search(r"\"MSE", name[i])
            or re.search(r"\"MSSB", name[i])
            or re.search(r"\"MST", name[i])
        ):
            p = patches.Rectangle(
                xy,
                l[i],
                heigth,
                fill=True,
                edgecolor="magenta",
                facecolor="w",
                lw=1,
                alpha=1,
                clip_on=False,
            )
            plt.gca().add_patch(p)
        if re.search(r"\"ZS", name[i]):
            p = patches.Rectangle(
                xy,
                l[i],
                heigth,
                fill=True,
                edgecolor="gray",
                facecolor="w",
                lw=1,
                alpha=1,
                clip_on=False,
            )
            plt.gca().add_patch(p)
        if re.search(r"\"MB", name[i]) or re.search(r"\"MD", name[i]):
            p = patches.Rectangle(
                xy,
                l[i],
                heigth * 0.9,
                fill=True,
                edgecolor="k",
                facecolor="k",
                lw=1,
                alpha=1,
                clip_on=False,
            )
            plt.gca().add_patch(p)
        if (re.search(r"\"MB", name[i]) or re.search(r"\"MD", name[i])) and "V" in name[
            i
        ]:
            p = patches.Rectangle(
                xy,
                l[i],
                heigth * 0.9,
                fill=True,
                edgecolor="magenta",
                facecolor="k",
                lw=1,
                alpha=1,
                clip_on=False,
            )
            plt.gca().add_patch(p)
        if re.search(r"\"MK", name[i]):
            p = patches.Rectangle(
                xy,
                l[i],
                heigth * 0.9,
                fill=True,
                edgecolor="r",
                facecolor="r",
                lw=1,
                alpha=1,
                clip_on=False,
            )
            plt.gca().add_patch(p)
        if re.search(r"\"CRY", name[i]):
            p = patches.Rectangle(
                xy,
                l[i],
                heigth * 0.3,
                fill=True,
                edgecolor="lime",
                facecolor="none",
                lw=1,
                alpha=1,
                clip_on=False,
            )
            plt.gca().add_patch(p)
        if (
            re.search(r"\"TAL", name[i])
            or re.search(r"\"TCS", name[i])
            or re.search(r"\"TID", name[i])
        ):
            p1 = patches.Rectangle(
                xy,
                l[i],
                heigth * 0.9,
                fill=True,
                edgecolor="gray",
                facecolor="gray",
                lw=1,
                alpha=1,
                clip_on=False,
            )
            plt.gca().add_patch(p1)
        if re.search(r"\"BTV", name[i]):
            p1 = patches.Rectangle(
                xy,
                l[i] * 0.2,
                heigth * 0.9,
                fill=True,
                edgecolor="yellow",
                facecolor="yellow",
                lw=1,
                alpha=1,
                clip_on=False,
            )
            plt.gca().add_patch(p1)
        if re.search(r"\"TAC", name[i]):
            p1 = patches.Rectangle(
                xy_qd,
                l[i],
                heigth * 0.5,
                fill=True,
                edgecolor="gray",
                facecolor="gray",
                lw=1,
                alpha=1,
                clip_on=False,
            )
            plt.gca().add_patch(p1)
        if re.search(r"\"AC", name[i]):
            p1 = patches.Rectangle(
                xy_qd,
                l[i],
                heigth * 0.5,
                fill=True,
                edgecolor="green",
                facecolor="green",
                lw=1,
                alpha=1,
                clip_on=False,
            )
            plt.gca().add_patch(p1)

    plt.axhline(y=pos_y, lw=0.5, alpha=0.8, c="black", clip_on=False)


def plot_line_seq_lhc(name, s, l, pos_y, heigth):
    """

    @param twiss: twiss file path
    @param pos_y: where to plot the beamline schematic view
    @param heigth: dimension of the sketch
    """

    for i in range(1, len(s)):
        x = s[i] - l[i]
        y = pos_y - heigth / 2
        xy = x, y
        xy_qd = x, y - heigth
        xy_qf = x, y + heigth / 2
        if re.search(r"\"MQ", name[i]):
            p = patches.Rectangle(
                xy_qf,
                l[i],
                heigth * 1.3,
                fill=True,
                edgecolor="magenta",
                facecolor="w",
                lw=1,
                alpha=1,
            )
            plt.gca().add_patch(p)
        if re.search(r"\"TC", name[i]) or re.search(r"\"TD", name[i]):
            p = patches.Rectangle(
                xy, l[i], heigth, fill=True, edgecolor="k", facecolor="k", lw=1, alpha=1
            )
            plt.gca().add_patch(p)
        if re.search(r"\"MB\.", name[i]):
            p = patches.Rectangle(
                xy,
                l[i],
                heigth * 0.9,
                fill=True,
                edgecolor="lime",
                facecolor="k",
                lw=1,
                alpha=1,
            )
            plt.gca().add_patch(p)
        if re.search(r"\"MBX\.", name[i]):
            p = patches.Rectangle(
                xy,
                l[i],
                heigth * 0.9,
                fill=True,
                edgecolor="red",
                facecolor="k",
                lw=1,
                alpha=1,
            )
            plt.gca().add_patch(p)
    plt.axhline(y=pos_y, lw=0.5, alpha=0.8, c="black")


def read_sps_yasp_file(file_name):
    data = np.genfromtxt(
        file_name, names=True, comments="*", dtype=None, skip_header=28, skip_footer=218
    )

    co_ref_x = []
    co_ref_y = []

    bpm_h_list = []
    bpm_v_list = []
    num_f = 0

    for j in range(len(data["STATUS"])):
        if (
            data["STATUS"][j] == 0
            and data["PLANE"][j] == "H"
            and not (re.search("BPCE.", data["NAME"][j]))
        ):
            co_ref_x.append(data["POS"][j])
            bpm_h_list.append(data["NAME"][j])

        if (
            data["STATUS"][j] == 0
            and data["PLANE"][j] == "V"
            and not (re.search("BPCE.", data["NAME"][j]))
        ):
            co_ref_y.append(data["POS"][j])
            bpm_v_list.append(data["NAME"][j])

    co_ref_x = np.array(co_ref_x) * 1e-6
    co_ref_y = np.array(co_ref_y) * 1e-6

    return co_ref_x, bpm_h_list, co_ref_y, bpm_v_list


def read_tl_yasp_file(file_name):
    # data = np.genfromtxt(file_name, names=True, comments='*', dtype=None, skip_header=29, skip_footer=152)
    data = np.genfromtxt(
        file_name, names=True, comments="*", dtype=None, skip_header=29, skip_footer=52
    )

    co_ref_x = []
    co_ref_y = []

    bpm_h_list = []
    bpm_v_list = []
    num_f = 0

    print(data["NAME"])

    for j in range(len(data["STATUS"])):
        if (
            data["STATUS"][j] == 0
            and data["PLANE"][j] == "H"
            and not (re.search("BPCE.", data["NAME"][j]))
        ):
            co_ref_x.append(data["POS"][j])
            bpm_h_list.append(data["NAME"][j])

        if (
            data["STATUS"][j] == 0
            and data["PLANE"][j] == "V"
            and not (re.search("BPCE.", data["NAME"][j]))
        ):
            co_ref_y.append(data["POS"][j])
            bpm_v_list.append(data["NAME"][j])

    co_ref_x = np.array(co_ref_x) * 1e-6
    co_ref_y = np.array(co_ref_y) * 1e-6

    return co_ref_x, bpm_h_list, co_ref_y, bpm_v_list


def read_sps_yasp_file_complete(file_name):
    data = np.genfromtxt(
        file_name, names=True, comments="*", dtype=None, skip_header=28, skip_footer=218
    )

    co_ref_x = []
    co_ref_y = []

    bpm_h_list = []
    bpm_v_list = []
    num_f = 0

    for j in range(len(data["STATUS"])):
        if (
            data["STATUS"][j] == 0
            and data["PLANE"][j] == "H"
            and not (re.search("BPCE.[1,2]", data["NAME"][j]))
        ):
            co_ref_x.append(data["POS"][j])
            bpm_h_list.append(data["NAME"][j])

        if (
            data["STATUS"][j] == 0
            and data["PLANE"][j] == "V"
            and not (re.search("BPCE.[1,2]", data["NAME"][j]))
        ):
            co_ref_y.append(data["POS"][j])
            bpm_v_list.append(data["NAME"][j])

    co_ref_x = np.array(co_ref_x) * 1e-6
    co_ref_y = np.array(co_ref_y) * 1e-6

    return co_ref_x, bpm_h_list, co_ref_y, bpm_v_list


def get_aper_block(element_index, s_list, aper_list, l_list, misa_up, misa_do):
    elem_s = [
        s_list[element_index] - l_list[element_index],
        s_list[element_index],
        s_list[element_index],
        s_list[element_index] - l_list[element_index],
        s_list[element_index] - l_list[element_index],
    ]
    elem_up = np.array(
        [
            aper_list[element_index] + misa_up,
            aper_list[element_index] + misa_do,
            0.1,
            0.1,
            aper_list[element_index] + misa_up,
        ]
    )

    elem_do = -1 * np.array(
        [
            aper_list[element_index] - misa_up,
            aper_list[element_index] - misa_do,
            0.1,
            0.1,
            aper_list[element_index] - misa_up,
        ]
    )

    return elem_s, elem_up, elem_do


def plot_aper_block(
    element_index,
    s_list,
    aper_list,
    l_list,
    misa_up,
    misa_do,
    color,
    label,
    delta_p=0.0,
    delta_n=0.0,
):
    elem_s = [
        s_list[element_index] - l_list[element_index],
        s_list[element_index],
        s_list[element_index],
        s_list[element_index] - l_list[element_index],
        s_list[element_index] - l_list[element_index],
    ]
    elem_up = np.array(
        [
            aper_list[element_index] + misa_up,
            aper_list[element_index] + misa_do,
            0.3,
            0.3,
            aper_list[element_index] + misa_up,
        ]
    )

    elem_do = -1 * np.array(
        [
            aper_list[element_index] - misa_up,
            aper_list[element_index] - misa_do,
            0.3,
            0.3,
            aper_list[element_index] - misa_up,
        ]
    )

    plt.fill(elem_s, elem_up + delta_p, color=color, label=label)
    plt.fill(elem_s, elem_do + delta_n, color=color)


def plot_aper_block_nl(
    element_index, s_list, aper_list, l_list, misa_up, misa_do, color
):
    elem_s = [
        s_list[element_index] - l_list[element_index],
        s_list[element_index],
        s_list[element_index],
        s_list[element_index] - l_list[element_index],
        s_list[element_index] - l_list[element_index],
    ]
    elem_up = np.array(
        [
            aper_list[element_index] + misa_up,
            aper_list[element_index] + misa_do,
            0.1,
            0.1,
            aper_list[element_index] + misa_up,
        ]
    )

    elem_do = -1 * np.array(
        [
            aper_list[element_index] - misa_up,
            aper_list[element_index] - misa_do,
            0.1,
            0.1,
            aper_list[element_index] - misa_up,
        ]
    )

    plt.fill(elem_s, elem_up, color=color)
    plt.fill(elem_s, elem_do, color=color)


def get_sequencetwiss(twiss_file):
    with open(twiss_file, "r") as f:
        for i, s in enumerate(f):
            if i == 2:
                sequence = s.split()[3].strip('"')
    return sequence


def lists2madxtable(list_1, list_2, list_3, outputfile, plane):
    """
    Writes 3 lists to a madx-like file.

    @param list_1, list_2, list_3: 3 lists
    @param type: 'X' or 'Y'
    """

    normal = PrettyTable(["* NAME", "S", "X", "Y"])
    normal.align = "l"
    normal.border = False
    normal.add_row(["$ %s", "%le", "%le", "%le"])

    head_line1 = '@ TYPE             %04s "USER"\n'
    head_line2 = '@ FileCreated      %28s "{0}"\n'.format(
        datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
    )
    head_line3 = '@ Name             %007 "' + plane + 'TARGET"\n'
    head = head_line1 + head_line2 + head_line3

    if plane == "X":
        with open(outputfile + ".tfs", "w") as fp:
            for i in range(len(head)):
                fp.write(head[i])
            for i in range(len(list_1)):
                normal.add_row([list_1[i], list_2[i], list_3[i], "0.0"])
            fp.write(normal.get_string())
    else:
        with open(outputfile + ".tfs", "w") as fp:
            for i in range(len(head)):
                fp.write(head[i])
            for i in range(len(list_1)):
                normal.add_row([list_1[i], list_2[i], "0.0", list_3[i]])
            fp.write(normal.get_string())


def lists2tfstable(lists, names, outputfile):

    names[0] = "*                      " + names[0]
    normal = PrettyTable()
    normal.align = "l"
    normal.border = False
    types_list = ["%le"] * len(names)

    types_list[0] = "$                    " + types_list[0]

    for j in range(len(lists)):
        lists[j] = [types_list[j]] + list(lists[j])

    for i in range(len(names)):
        normal.add_column(names[i].upper(), lists[i], align="r")

    head_line1 = '@ TYPE             %04s "USER"\n'
    head_line2 = '@ FileCreated      %28s "{0}"\n'.format(
        datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
    )
    head_line3 = '@ Name             %007 MYTABLE"\n'
    head = head_line1 + head_line2 + head_line3

    with open(outputfile + ".tfs", "w") as fp:
        for i in range(len(head)):
            fp.write(head[i])
        fp.write(normal.get_string())


def string_to_float(seq):
    for x in seq:
        try:
            yield float(x)
        except ValueError:
            yield x


def get_fat_halo(
    input="sequence_totrack.tfs", n_halo=(4, 5), beam_t="LHC", n_part=100, seed=123
):
    twiss_file = input

    variables = linecache.getline(twiss_file, 46).split()[1:]
    twiss = list(string_to_float(linecache.getline(twiss_file, 48).split()))

    x0 = twiss[variables.index("X")]
    y0 = twiss[variables.index("Y")]
    px0 = twiss[variables.index("PX")]
    py0 = twiss[variables.index("PY")]

    betx = twiss[variables.index("BETX")]
    bety = twiss[variables.index("BETY")]

    alfx = twiss[variables.index("ALFX")]
    alfy = twiss[variables.index("ALFY")]

    dx = twiss[variables.index("DX")]
    dpx = twiss[variables.index("DPX")]
    dy = twiss[variables.index("DY")]
    dpy = twiss[variables.index("DPY")]

    # ========================================
    # Seed
    # ========================================

    np.random.seed(seed)
    random.seed(seed)

    beam_type = beam_t

    if beam_type == "LHC":
        beam = dict(
            type="LHC",
            energy=450,
            emit_nom=3.5e-6,
            emit_n=3.5e-6,
            intensity=288 * 1.15e11,
            dpp_t=3e-4,
            n_particles=10000,
            n_sigma=5,
        )
    elif beam_type == "LIU":
        beam = dict(
            type="LIU",
            energy=450,
            emit_nom=3.5e-6,
            emit_n=1.37e-6,
            intensity=288 * 2.0e11,
            dpp_t=3e-4,
            n_particles=1000,
            n_sigma=5,
        )
    elif beam_type == "LHC-meas":
        beam = dict(
            type="LHC-meas",
            energy=450,
            emit_nom=3.5e-6,
            emit_n=2.0e-6,
            intensity=288 * 1.2e11,
            dpp_t=3e-4,
            n_particles=1e6,
            n_sigma=6.8,
        )
    elif beam_type == "HL-LHC":
        beam = dict(
            type="HL-LHC",
            energy=450,
            emit_nom=3.5e-6,
            emit_n=2.08e-6,
            intensity=288 * 2.32e11,
            dpp_t=3e-4,
            n_particles=1e6,
            n_sigma=5,
        )
    elif beam_type == "coast":
        beam = dict(
            type="coast",
            energy=270,
            emit_nom=3.5e-6,
            emit_n=3.5e-6,
            intensity=1e12,
            dpp_t=3e-4,
            n_particles=1e6,
            n_sigma=5,
        )
    elif beam_type == "FT":
        beam = dict(
            type="FT",
            energy=400,
            emit_nom=8e-6,
            emit_n=8e-6,
            intensity=1e12,
            dpp_t=4e-4,
            n_particles=1e6,
            n_sigma=5,
        )

    emit_n = beam["emit_n"]  # [mm.mrad]
    dpp_t = beam["dpp_t"]

    n = n_part

    m_p = 0.938

    # ========================================
    # Beam parameters
    # ========================================

    p = beam["energy"]  # momentum in GeV

    gamma = np.sqrt(m_p ** 2 + p ** 2) / m_p
    beta0 = np.sqrt(gamma ** 2 - 1) / gamma
    beta_r = np.sqrt(1.0 - 1.0 / gamma ** 2)
    emit_x = emit_n / (beta_r * gamma)
    emit_y = emit_n / (beta_r * gamma)

    e0 = np.sqrt(p ** 2 + m_p ** 2)

    # ========================================
    # Delta p / p
    # ========================================

    ddp = (np.random.rand(n) * 2.0 * dpp_t) - dpp_t

    de = ddp * e0 * beta0 ** 2
    pt = de / p
    # ==============================================
    # Halo distributions
    # ==============================================

    psi = np.random.rand(n) * 2.0 * np.pi

    ddp_t = dpp_t

    width = np.array([random.uniform(n_halo[0], n_halo[1]) for i in range(0, n)])

    x1 = x0 + width * np.sqrt(betx * emit_x) * np.cos(psi) + (dx * ddp_t)
    px1 = (
        px0
        - (width * np.sqrt(emit_x / betx) * (np.sin(psi) + alfx * np.cos(psi)))
        + (dpx * ddp_t)
    )

    psiy = np.random.rand(n) * 2 * np.pi

    y1 = y0 + width * np.sqrt(bety * emit_y) * np.cos(psiy) + (dy * ddp_t)
    py1 = (
        py0
        - (width * np.sqrt(emit_y / bety) * (np.sin(psiy) + alfy * np.cos(psiy)))
        + (dpy * ddp_t)
    )

    t = np.zeros(n)

    return x1, px1, y1, py1, t, pt


def get_fat_halo_from_input(beta, alfa, n_halo=(4, 5), n_part=100, seed=123):

    x0 = 0.0
    y0 = 0.0
    px0 = 0.0
    py0 = 0.0

    betx = beta
    bety = beta

    alfx = alfa
    alfy = alfa

    # ========================================
    # Seed
    # ========================================

    np.random.seed(seed)
    random.seed(seed)

    # ==============================================
    # Halo distributions
    # ==============================================

    psi = np.random.rand(n) * 2.0 * np.pi

    width = np.array([random.uniform(n_halo[0], n_halo[1]) for i in range(0, n)])

    x1 = x0 + width * np.sqrt(betx * emit_x) * np.cos(psi) + (dx * ddp_t)
    px1 = (
        px0
        - (width * np.sqrt(emit_x / betx) * (np.sin(psi) + alfx * np.cos(psi)))
        + (dpx * ddp_t)
    )

    psiy = np.random.rand(n) * 2 * np.pi

    y1 = y0 + width * np.sqrt(bety * emit_y) * np.cos(psiy) + (dy * ddp_t)
    py1 = (
        py0
        - (width * np.sqrt(emit_y / bety) * (np.sin(psiy) + alfy * np.cos(psiy)))
        + (dpy * ddp_t)
    )

    t = np.zeros(n)

    return x1, px1, y1, py1, t


def get_gauss_distribution(
    input="sequence_totrack.tfs", sigmas=3, beam_t="LHC", n_part=100, seed=123
):
    twiss_file = input

    twiss, _ = get_twiss_dic(twiss_file)

    x0 = twiss["X"][0]
    y0 = twiss["Y"][0]
    px0 = twiss["PX"][0]
    py0 = twiss["PY"][0]

    betx = twiss["BETX"][0]
    bety = twiss["BETY"][0]

    alfx = twiss["ALFX"][0]
    alfy = twiss["ALFY"][0]

    dx = twiss["DX"][0]
    dpx = twiss["DPX"][0]
    dy = twiss["DY"][0]
    dpy = twiss["DPY"][0]

    # ========================================
    # Seed
    # ========================================

    np.random.seed(seed)

    beam_type = beam_t

    if beam_type == "LHC":
        beam = dict(
            type="LHC",
            energy=450,
            emit_nom=3.5e-6,
            emit_n=3.5e-6,
            intensity=288 * 1.15e11,
            dpp_t=3e-4,
            n_particles=10000,
            n_sigma=5,
        )
    elif beam_type == "LIU":
        beam = dict(
            type="LIU",
            energy=450,
            emit_nom=3.5e-6,
            emit_n=1.37e-6,
            intensity=288 * 2.0e11,
            dpp_t=3e-4,
            n_particles=1000,
            n_sigma=5,
        )
    elif beam_type == "LHC-meas":
        beam = dict(
            type="LHC-meas",
            energy=450,
            emit_nom=3.5e-6,
            emit_n=2.0e-6,
            intensity=288 * 1.2e11,
            dpp_t=3e-4,
            n_particles=1e6,
            n_sigma=6.8,
        )
    elif beam_type == "HL-LHC":
        beam = dict(
            type="HL-LHC",
            energy=450,
            emit_nom=3.5e-6,
            emit_n=2.08e-6,
            intensity=288 * 2.32e11,
            dpp_t=3e-4,
            n_particles=1e6,
            n_sigma=5,
        )
    elif beam_type == "coast":
        beam = dict(
            type="coast",
            energy=270,
            emit_nom=3.5e-6,
            emit_n=3.5e-6,
            intensity=1e12,
            dpp_t=3e-4,
            n_particles=1e6,
            n_sigma=5,
        )

    elif beam_type == "FT":
        beam = dict(
            type="FT",
            energy=400,
            emit_nom=8e-6,
            emit_n=8e-6,
            intensity=1e12,
            dpp_t=4e-4,
            n_particles=1e6,
            n_sigma=5,
        )
    n_sigma = sigmas

    emit_n = beam["emit_n"]  # [mm.mrad]
    dpp_t = beam["dpp_t"]

    n = n_part

    m_p = 0.938

    # ========================================
    # Beam parameters
    # ========================================

    p = beam["energy"]  # momentum in GeV

    gamma = np.sqrt(m_p ** 2 + p ** 2) / m_p
    beta0 = np.sqrt(gamma ** 2 - 1) / gamma
    beta_r = np.sqrt(1.0 - 1.0 / gamma ** 2)
    emit_x = emit_n / (beta_r * gamma)
    emit_y = emit_n / (beta_r * gamma)

    e0 = np.sqrt(p ** 2 + m_p ** 2)

    # ========================================
    # Delta p / p
    # ========================================

    ddp = (np.random.rand(n) * 2.0 * dpp_t) - dpp_t

    de = ddp * e0 * beta0 ** 2
    pt = de / p

    # ==============================================
    # Bivariate normal distributions
    # ==============================================

    sx = np.sqrt(emit_x * betx)
    n_x = trandn(-1 * n_sigma, n_sigma, scale=sx).rvs(n)
    x = x0 + n_x + (dx * ddp)
    px = (
        px0
        + (trandn(-1 * n_sigma, n_sigma, scale=sx).rvs(n) - alfx * n_x) / betx
        + (dpx * ddp)
    )

    sy = np.sqrt(emit_y * bety)
    n_y = trandn(-1 * n_sigma, n_sigma, scale=sy).rvs(n)
    y = y0 + n_y + (dy * ddp)
    py = (
        py0
        + (trandn(-1 * n_sigma, n_sigma, scale=sy).rvs(n) - alfy * n_y) / bety
        + (dpy * ddp)
    )

    t = np.zeros(n)

    return x, px, y, py, t, pt


def get_distribution_electrons(
    input="sequence_totrack.tfs",
    sigmas=3,
    dist_tr="Gauss",
    dist_en="Uni",
    beam_t="ELINE",
    n_part=100,
    seed=123,
    sigma_p=0.0,
    emit_x=2e-6,
    emit_y=2e-6,
):
    twiss_file = input

    variables = linecache.getline(twiss_file, 46).split()[1:]
    twiss = list(string_to_float(linecache.getline(twiss_file, 48).split()))

    x0 = twiss[variables.index("X")]
    y0 = twiss[variables.index("Y")]
    px0 = twiss[variables.index("PX")]
    py0 = twiss[variables.index("PY")]

    betx = twiss[variables.index("BETX")]
    bety = twiss[variables.index("BETY")]

    alfx = twiss[variables.index("ALFX")]
    alfy = twiss[variables.index("ALFY")]

    dx = twiss[variables.index("DX")]
    dpx = twiss[variables.index("DPX")]
    dy = twiss[variables.index("DY")]
    dpy = twiss[variables.index("DPY")]

    # ========================================
    # Seed
    # ========================================

    np.random.seed(seed)

    beam_type = beam_t

    if beam_type == "ELINE":
        beam = dict(
            type="ELINE",
            energy=17.0e-3,
            emit_nom=emit_x,
            emit_n_x=emit_x,
            emit_n_y=emit_y,
            intensity=288 * 1.15e11,
            sigma_p=sigma_p,
            n_particles=10000,
            n_sigma=5,
        )

    n_sigma = sigmas

    emit_n_x = beam["emit_n_x"]  # [mm.mrad]
    emit_n_y = beam["emit_n_y"]  # [mm.mrad]

    n = n_part

    m_p = 0.511e-3

    # ========================================
    # Beam parameters
    # ========================================

    p = beam["energy"]  # momentum in GeV

    gamma = np.sqrt(m_p ** 2 + p ** 2) / m_p
    beta0 = np.sqrt(gamma ** 2 - 1) / gamma
    beta_r = np.sqrt(1.0 - 1.0 / gamma ** 2)
    emit_x = emit_n_x / (beta_r * gamma)
    emit_y = emit_n_y / (beta_r * gamma)

    e0 = np.sqrt(p ** 2 + m_p ** 2)

    # ========================================
    # Delta p / p
    # ========================================

    # ddp = trandn(-1 * n_sigma, n_sigma, scale=beam['sigma_p']).rvs(n)

    dpp_sigma = beam["sigma_p"]
    ddp = (np.random.gamma(2, dpp_sigma / np.sqrt(2), n)) - (
        np.sqrt(2 * dpp_sigma ** 2)
    )
    # ddp = (np.random.rand(n) * 2. * beam['sigma_p']) - beam['sigma_p']

    de = ddp * e0 * beta0 ** 2
    pt = de / p

    # ==============================================
    # Bivariate normal distributions
    # ==============================================

    sx = np.sqrt(emit_x * betx)
    n_x = trandn(-1 * n_sigma, n_sigma, scale=sx).rvs(n)
    x = x0 + n_x + (dx * ddp)
    px = (
        px0
        + (trandn(-1 * n_sigma, n_sigma, scale=sx).rvs(n) - alfx * n_x) / betx
        + (dpx * ddp)
    )

    sy = np.sqrt(emit_y * bety)
    n_y = trandn(-1 * n_sigma, n_sigma, scale=sy).rvs(n)
    y = y0 + n_y + (dy * ddp)
    py = (
        py0
        + (trandn(-1 * n_sigma, n_sigma, scale=sy).rvs(n) - alfy * n_y) / bety
        + (dpy * ddp)
    )

    t = np.zeros(n)

    return x, px, y, py, t, pt


def get_gauss_distribution_electrons(
    input="sequence_totrack.tfs",
    sigmas=3,
    beam_t="ELINE",
    n_part=100,
    seed=123,
    sigma_p=0.0,
    emit_x=2e-6,
    emit_y=2e-6,
    p=15e-3,
):
    twiss_file = input

    variables = linecache.getline(twiss_file, 46).split()[1:]
    twiss = list(string_to_float(linecache.getline(twiss_file, 48).split()))

    x0 = twiss[variables.index("X")]
    y0 = twiss[variables.index("Y")]
    px0 = twiss[variables.index("PX")]
    py0 = twiss[variables.index("PY")]

    betx = twiss[variables.index("BETX")]
    bety = twiss[variables.index("BETY")]

    alfx = twiss[variables.index("ALFX")]
    alfy = twiss[variables.index("ALFY")]

    dx = twiss[variables.index("DX")]
    dpx = twiss[variables.index("DPX")]
    dy = twiss[variables.index("DY")]
    dpy = twiss[variables.index("DPY")]

    # ========================================
    # Seed
    # ========================================

    np.random.seed(seed)

    beam_type = beam_t

    if beam_type == "ELINE":
        beam = dict(
            type="ELINE",
            energy=p,
            emit_nom=emit_x,
            emit_n_x=emit_x,
            emit_n_y=emit_y,
            intensity=288 * 1.15e11,
            sigma_p=sigma_p,
            n_particles=10000,
            n_sigma=5,
        )

    n_sigma = sigmas

    emit_n_x = beam["emit_n_x"]  # [mm.mrad]
    emit_n_y = beam["emit_n_y"]  # [mm.mrad]

    n = n_part

    m_p = 0.511e-3

    # ========================================
    # Beam parameters
    # ========================================

    p = beam["energy"]  # momentum in GeV

    gamma = np.sqrt(m_p ** 2 + p ** 2) / m_p
    beta0 = np.sqrt(gamma ** 2 - 1) / gamma
    beta_r = np.sqrt(1.0 - 1.0 / gamma ** 2)
    emit_x = emit_n_x / (beta_r * gamma)
    emit_y = emit_n_y / (beta_r * gamma)

    e0 = np.sqrt(p ** 2 + m_p ** 2)

    # ========================================
    # Delta p / p
    # ========================================

    ddp = trandn(-1 * n_sigma, n_sigma, scale=beam["sigma_p"]).rvs(n)

    dpp_sigma = beam["sigma_p"]
    # ddp = (np.random.gamma(2, dpp_sigma / np.sqrt(2), n)) - (np.sqrt(2 * dpp_sigma**2))
    # ddp = (np.random.rand(n) * 2. * beam['sigma_p']) - beam['sigma_p']

    de = ddp * e0 * beta0 ** 2
    pt = de / p

    # ==============================================
    # Bivariate normal distributions
    # ==============================================

    sx = np.sqrt(emit_x * betx)
    n_x = trandn(-1 * n_sigma, n_sigma, scale=sx).rvs(n)
    x = x0 + n_x + (dx * ddp)
    px = (
        px0
        + (trandn(-1 * n_sigma, n_sigma, scale=sx).rvs(n) - alfx * n_x) / betx
        + (dpx * ddp)
    )

    sy = np.sqrt(emit_y * bety)
    n_y = trandn(-1 * n_sigma, n_sigma, scale=sy).rvs(n)
    y = y0 + n_y + (dy * ddp)
    py = (
        py0
        + (trandn(-1 * n_sigma, n_sigma, scale=sy).rvs(n) - alfy * n_y) / bety
        + (dpy * ddp)
    )

    t = np.zeros(n)

    return x, px, y, py, t, pt


def writeDistributionFile(planes, filename_out):

    x1, px1, y1, py1, t, pt = planes

    head = []
    head.append('@ TYPE          %04s "USER"\n')
    head.append('@ FileCreated   %28s "' + str(datetime.now()) + '"\n')
    head.append('@ Name          %007 "MYTABLE"\n')

    # file_head = 'initial_distribution.txt'
    # head = []
    # with open(file_head, 'r') as ff:
    #     for i in xrange(6):
    #         head.append(ff.readline())

    halo_tab = PrettyTable(
        ["*", "NUMBER", "TURN", "X", "PX", "Y", "PY", "T", "PT", "S", "E"]
    )
    halo_tab.align = "r"
    halo_tab.border = False
    halo_tab.add_row(
        ["$", "%d", "%d", "%le", "%le", "%le", "%le", "%le", "%le", "%le", "%le"]
    )

    with open(filename_out + ".txt", "w") as fp:
        for i in range(len(head)):
            fp.write(head[i])
        for i in range(len(x1)):
            halo_tab.add_row(
                [
                    " ",
                    i + 1,
                    0,
                    x1[i],
                    px1[i],
                    y1[i],
                    py1[i],
                    0.0000000,
                    pt[i],
                    0.0000000,
                    450.000978170460,
                ]
            )
        fp.write(halo_tab.get_string())


def rand_hole(s1, s2, loc, scale, n):
    x1 = trandn(-s2, -s1, loc=loc, scale=scale).rvs(n / 2)
    x2 = trandn(s1, s2, loc=loc, scale=scale).rvs(n / 2)
    x = list(x1) + list(x2)
    shuffle(x)
    return np.array(x)


def get_gauss_halo_distribution(
    input="sequence_totrack.tfs",
    sigmas=(1, 3),
    beam_t="LHC",
    n_part=100,
    seed=123,
    dpp_dstr="norm",
    dpp_sigma=5,
):
    twiss_file = input

    variables = linecache.getline(twiss_file, 46).split()[1:]
    twiss = list(string_to_float(linecache.getline(twiss_file, 48).split()))

    x0 = twiss[variables.index("X")]
    y0 = twiss[variables.index("Y")]
    px0 = twiss[variables.index("PX")]
    py0 = twiss[variables.index("PY")]

    betx = twiss[variables.index("BETX")]
    bety = twiss[variables.index("BETY")]

    alfx = twiss[variables.index("ALFX")]
    alfy = twiss[variables.index("ALFY")]

    dx = twiss[variables.index("DX")]
    dpx = twiss[variables.index("DPX")]
    dy = twiss[variables.index("DY")]
    dpy = twiss[variables.index("DPY")]

    # ========================================
    # Seed
    # ========================================

    np.random.seed(seed)

    beam_type = beam_t

    if beam_type == "LHC":
        beam = dict(
            type="LHC",
            energy=450,
            emit_nom=3.5e-6,
            emit_n=3.5e-6,
            intensity=288 * 1.15e11,
            dpp_t=3e-4,
            n_particles=10000,
            n_sigma=5,
        )
    elif beam_type == "LIU":
        beam = dict(
            type="LIU",
            energy=450,
            emit_nom=3.5e-6,
            emit_n=1.37e-6,
            intensity=288 * 2.0e11,
            dpp_t=3e-4,
            n_particles=1000,
            n_sigma=5,
        )
    elif beam_type == "LHC-meas":
        beam = dict(
            type="LHC-meas",
            energy=450,
            emit_nom=3.5e-6,
            emit_n=2.0e-6,
            intensity=288 * 1.2e11,
            dpp_t=3e-4,
            n_particles=1e6,
            n_sigma=6.8,
        )
    elif beam_type == "HL-LHC":
        beam = dict(
            type="HL-LHC",
            energy=450,
            emit_nom=3.5e-6,
            emit_n=2.08e-6,
            intensity=288 * 2.32e11,
            dpp_t=3e-4,
            n_particles=1e6,
            n_sigma=5,
        )
    elif beam_type == "coast":
        beam = dict(
            type="coast",
            energy=270,
            emit_nom=3.5e-6,
            emit_n=3.5e-6,
            intensity=1e12,
            dpp_t=3e-4,
            n_particles=1e6,
            n_sigma=5,
        )
    elif beam_type == "FT":
        beam = dict(
            type="FT",
            energy=400,
            emit_nom=8e-6,
            emit_n=8e-6,
            intensity=1e12,
            dpp_t=4e-4,
            n_particles=1e6,
            n_sigma=5,
        )

    n_sigma = sigmas

    emit_n = beam["emit_n"]  # [mm.mrad]
    dpp_t = beam["dpp_t"]

    n = n_part

    m_p = 0.938

    # ========================================
    # Beam parameters
    # ========================================

    p = beam["energy"]  # momentum in GeV

    gamma = np.sqrt(m_p ** 2 + p ** 2) / m_p
    beta0 = np.sqrt(gamma ** 2 - 1) / gamma
    beta_r = np.sqrt(1.0 - 1.0 / gamma ** 2)
    emit_x = emit_n / (beta_r * gamma)
    emit_y = emit_n / (beta_r * gamma)

    e0 = np.sqrt(p ** 2 + m_p ** 2)

    # ========================================
    # Delta p / p
    # ========================================
    if dpp_dstr == "uni":
        ddp = (np.random.rand(n) * 2.0 * dpp_t) - dpp_t

    elif dpp_dstr == "norm":
        ddp = trandn(-5, 5, loc=0.0, scale=dpp_sigma).rvs(n)
    # ddp = trandn(-5, 5, loc=0.0, scale=dpp_t).rvs(n)
    de = np.array(ddp) * e0 * beta0 ** 2
    pt = de / p

    # ==============================================
    # Bivariate normal distributions
    # ==============================================

    min_s = sigmas[0] / np.sqrt(2)
    max_s = sigmas[1] / np.sqrt(2)

    r_x = np.sqrt(emit_x) * np.sqrt(
        trandn(min_s, max_s, loc=0, scale=1).rvs(n_part) ** 2
        + trandn(min_s, max_s, loc=0, scale=1).rvs(n_part) ** 2
    )
    r_y = np.sqrt(emit_y) * np.sqrt(
        trandn(min_s, max_s, loc=0, scale=1).rvs(n_part) ** 2
        + trandn(min_s, max_s, loc=0, scale=1).rvs(n_part) ** 2
    )

    t = np.linspace(0, 2 * np.pi, n_part)
    shuffle(t)

    x_n = r_x * np.cos(t)
    px_n = r_x * np.sin(t)

    y_n = r_y * np.cos(t)
    py_n = r_y * np.sin(t)

    x = x0 + (x_n * np.sqrt(betx)) + (dx * ddp)
    px = px0 + (px_n - alfx * x_n) / np.sqrt(betx) + (dy * ddp)

    y = y0 + (y_n * np.sqrt(bety)) + (dy * ddp)
    py = py0 + (py_n - alfy * y_n) / np.sqrt(bety) + (dy * ddp)

    t = np.zeros(n)

    return x, px, y, py, t, pt


def get_variables_turn(file_name, var1, var2, turn, element):
    variables = {var1: [], var2: []}

    ff = open(file_name, "r")
    for i in range(0, 53):
        if i == 51:
            var_list = ff.readline().split()
        else:
            ff.readline()

    col1 = var_list.index(var1.upper()) - 1
    col2 = var_list.index(var2.upper()) - 1
    count_turn = 0
    while True:
        line = ff.readline()
        if not line == "":
            if line[0] == "#":
                if element in line.split()[5] or element == line.split()[5]:
                    count_turn += 1
                    if turn == count_turn:
                        while True:
                            column = ff.readline().split()
                            if len(column) == 0:
                                break
                            if column[0][0] == "#":
                                break
                            variables[var1].append(float(column[col1]))
                            variables[var2].append(float(column[col2]))
                        break
        else:
            break
    ff.close()

    return np.array(variables[var1]), np.array(variables[var2])


def get_variables_turn_ptc(file_name, var1, var2, turn, element):
    variables = {var1: [], var2: []}

    ff = open(file_name, "r")
    for i in range(0, 9):
        if i == 6:
            var_list = ff.readline().split()
        else:
            ff.readline()

    col1 = var_list.index(var1.upper()) - 1
    col2 = var_list.index(var2.upper()) - 1
    count_turn = 0
    while True:
        line = ff.readline()
        if line[0] == "#":
            if element in line.split()[5]:
                count_turn += 1
                if turn == count_turn:
                    while True:
                        column = ff.readline().split()
                        if len(column) == 0:
                            break
                        if column[0][0] == "#":
                            break
                        variables[var1].append(float(column[col1]))
                        variables[var2].append(float(column[col2]))
                    break
    ff.close()

    return variables[var1], variables[var2]


def get_array_sdds(sdds_name, line_num, following_lines):
    temp = linecache.getline(sdds_name, line_num).split()

    for i in range(line_num + 1, line_num + following_lines + 1):
        temp = temp + linecache.getline(sdds_name, i).split()

    return np.array([float(element) for element in temp])


def find_pos_peaks(t, data, step):
    peaks = []
    t_of_peaks = []

    data_temp = []
    t_temp = []

    for i in range(0, len(data)):
        if data[i] > 0:
            data_temp.append(data[i])
            t_temp.append(t[i])

    for i in range(step, len(data_temp), step):
        peaks.append(max(data_temp[i - step : i]))
        t_of_peaks.append(
            float(t_temp[(i - step) + np.argmax(data_temp[i - step : i])])
        )

    return np.array(t_of_peaks), np.array(peaks)


def find_neg_peaks(t, data, step):
    peaks = []
    t_of_peaks = []

    data_temp = []
    t_temp = []

    for i in range(0, len(data)):
        if data[i] < 0:
            data_temp.append(data[i])
            t_temp.append(t[i])

    for i in range(step, len(data_temp), step):
        print(i)
        peaks.append(max(data_temp[i - step : i]))
        t_of_peaks.append(
            float(t_temp[(i - step) + np.argmax(data_temp[i - step : i])])
        )

    return np.array(t_of_peaks), np.array(peaks)


def check_se_apertures(s_loc, element_name, aperture, fig_num=None, device_len=1):
    """
    Calculation of the available aperture at a certain location in mm
    @param s_loc: s location of the place to check wrt nominal SPS files (s = 0 => SPS$START)
    @param element_name: name of the element
    @param aperture: aperture of the element to check (mm)
    @param figure: figure number
    @return: available aperture in mm wrt the envelope and raw separatrix
    """

    if platform.system() == "Linux":
        s, env_p, env_n = pickle.load(
            open(
                "/home/fvelotti/myScripts/Slow_extraction/pickles/separatrix_envelope_nominal.p",
                "r",
            )
        )
        _, x_p, x_n = pickle.load(
            open(
                "/home/fvelotti/myScripts/Slow_extraction/pickles/separatrix_raw_nominal.p",
                "r",
            )
        )
        aperx = (
            pickle.load(
                open(
                    "/home/fvelotti/myScripts/Slow_extraction/pickles/aperture_from_ZS_lss2.p",
                    "r",
                )
            )
            * 1e3
        )

    else:
        s, env_p, env_n = pickle.load(
            open(
                "/Users/fvelotti/myScripts/Slow_extraction/pickles/separatrix_envelope_nominal.p",
                "r",
            )
        )
        _, x_p, x_n = pickle.load(
            open(
                "/Users/fvelotti/myScripts/Slow_extraction/pickles/separatrix_raw_nominal.p",
                "r",
            )
        )
        aperx = (
            pickle.load(
                open(
                    "/Users/fvelotti/myScripts/Slow_extraction/pickles/aperture_from_ZS_lss2.p",
                    "r",
                )
            )
            * 1e3
        )

    env_n = np.array(env_n) * 1e3
    env_p = np.array(env_p) * 1e3

    x_n = np.array(x_n) * 1e3
    x_p = np.array(x_p) * 1e3

    sps_l = s[-1]

    eq_loc = s_loc - 1668.9776

    if eq_loc < 0:
        eq_loc += sps_l

    f_env_p = interp1d(s, env_p)
    f_env_n = interp1d(s, env_n)

    f_x_p = interp1d(s, x_p)
    f_x_n = interp1d(s, x_n)

    aper = interp1d(s, aperx)

    env = max(f_env_p(eq_loc), -1 * f_env_n(eq_loc))
    raw = max(f_x_p(eq_loc), -1 * f_x_n(eq_loc))

    if isinstance(fig_num, (int, float)):
        plt.figure(fig_num)

    plt.fill_between(s, x_n, x_p, alpha=1, color="magenta")

    plt.plot(s, env_n, "red")
    plt.plot(s, env_p, "red")

    plt.plot(s, aperx, "k")
    plt.plot(s, -1 * aperx, "k")

    plt.vlines(eq_loc, aperture, aper(eq_loc), "lime", lw=1, alpha=1)
    plt.vlines(eq_loc + device_len, aperture, aper(eq_loc), "lime", lw=1, alpha=1)

    plt.vlines(eq_loc, -1 * aperture, -1 * aper(eq_loc), "lime", lw=1, alpha=1)
    plt.vlines(
        eq_loc + device_len, -1 * aperture, -1 * aper(eq_loc), "lime", lw=1, alpha=1
    )

    plt.xlim(eq_loc - 500, eq_loc + 500)
    plt.ylim(-100, 100)

    plt.title(element_name)
    plt.xlabel("s / m")
    plt.ylabel("x / mm")

    return aperture - env, aperture - raw


def get_se_apertures(element_name):
    """
    Calculation of the acceptance at a known element
    @param s_loc: s location of the place to check wrt nominal SPS files (s = 0 => SPS$START)
    @param element_name: name of the element
    @param aperture: aperture of the element to check (mm)
    @param figure: figure number
    @return: available aperture in mm wrt the envelope and raw separatrix
    """

    if platform.system() == "Linux":
        s, env_p, env_n = pickle.load(
            open(
                "/home/fvelotti/myScripts/Slow_extraction/pickles/separatrix_envelope_nominal.p",
                "r",
            )
        )
        _, x_p, x_n = pickle.load(
            open(
                "/home/fvelotti/myScripts/Slow_extraction/pickles/separatrix_raw_nominal.p",
                "r",
            )
        )
        aperx = pickle.load(
            open(
                "/home/fvelotti/myScripts/Slow_extraction/pickles/aperture_from_ZS_lss2.p",
                "r",
            )
        )
        sigma_x, names = pickle.load(
            open(
                "/home/fvelotti/myScripts/Slow_extraction/pickles/sigma_x_from_ZS_lss2.p",
                "r",
            )
        )
    else:
        s, env_p, env_n = pickle.load(
            open(
                "/Users/fvelotti/myScripts/Slow_extraction/pickles/separatrix_envelope_nominal.p",
                "r",
            )
        )
        _, x_p, x_n = pickle.load(
            open(
                "/Users/fvelotti/myScripts/Slow_extraction/pickles/separatrix_raw_nominal.p",
                "r",
            )
        )
        aperx = pickle.load(
            open(
                "/Users/fvelotti/myScripts/Slow_extraction/pickles/aperture_from_ZS_lss2.p",
                "r",
            )
        )
        sigma_x, names = pickle.load(
            open(
                "/Users/fvelotti/myScripts/Slow_extraction/pickles/sigma_x_from_ZS_lss2.p",
                "r",
            )
        )

    env_n = np.array(env_n)
    env_p = np.array(env_p)

    x_n = np.array(x_n)
    x_p = np.array(x_p)

    eq_loc = names.index('"' + element_name + '"')

    env = max(env_p[eq_loc], -1 * env_n[eq_loc])
    raw = max(x_p[eq_loc], -1 * x_n[eq_loc])

    return aperx[eq_loc] - env, aperx[eq_loc] - raw


def plot_se_sepratrix(s_min, s_max, figure):
    """
    Calculation of the available aperture at a certain location in mm
    @param s_loc: s location of the place to check wrt nominal SPS files (s = 0 => SPS$START)
    @param element_name: name of the element
    @param aperture: aperture of the element to check (mm)
    @param figure: figure number
    @return: available aperture in mm wrt the envelope and raw separatrix
    """

    if platform.system() == "Linux":
        s, env_p, env_n = pickle.load(
            open(
                "/home/fvelotti/myScripts/Slow_extraction/pickles/separatrix_envelope_nominal.p",
                "r",
            )
        )
        _, x_p, x_n = pickle.load(
            open(
                "/home/fvelotti/myScripts/Slow_extraction/pickles/separatrix_raw_nominal.p",
                "r",
            )
        )
        aperx = pickle.load(
            open(
                "/home/fvelotti/myScripts/Slow_extraction/pickles/aperture_from_ZS_lss2.p",
                "r",
            )
        )
        s, env_p, env_n = pickle.load(
            open(
                "/Users/fvelotti/myScripts/Slow_extraction/pickles/separatrix_envelope_nominal.p",
                "r",
            )
        )
        _, x_p, x_n = pickle.load(
            open(
                "/Users/fvelotti/myScripts/Slow_extraction/pickles/separatrix_raw_nominal.p",
                "r",
            )
        )
        aperx = (
            pickle.load(
                open(
                    "/Users/fvelotti/myScripts/Slow_extraction/pickles/aperture_from_ZS_lss2.p",
                    "r",
                )
            )
            * 1e3
        )

    pos = list(s).index(5242.5262)

    s = cycle_s(s, pos)
    x_n = cycle_list(x_n, pos)
    x_p = cycle_list(x_p, pos)
    aperx = cycle_list(aperx, pos)

    x_n = np.array(x_n) * 1e3
    x_p = np.array(x_p) * 1e3

    plt.figure(figure)
    plt.fill_between(s, x_n, x_p, alpha=0.4, lw=0.5)
    # plt.plot(s, x_n, alpha=1, color='blue')
    # plt.plot(s, x_p, alpha=1, color='blue')

    # plot_aper(s, np.array(aperx), unit='mm')
    # plt.plot(s, aperx, 'k')
    # plt.plot(s, -1 * np.array(aperx), 'k')

    plt.xlim(s_min, s_max)

    return s, x_n, x_p


def cycle_list(list, pos):
    end_list = []
    beg_list = []
    beg = False

    for i in range(len(list)):
        if i == pos:
            beg = True

        if not beg:
            end_list.append(list[i])
        else:
            beg_list.append(list[i])

    list_new = beg_list + end_list

    return list_new


def cycle_s(list, pos):
    end_list = []
    beg_list = []
    beg = False

    s0 = list[pos]
    s_f = list[-1]

    for i in range(len(list)):
        if i == pos:
            beg = True

        if not beg:
            end_list.append(list[i] + s_f - s0)
        else:
            beg_list.append(list[i] - s0)

    list_new = beg_list + end_list

    return list_new


def get_separatrix():
    """
    Calculation of the available aperture at a certain location in mm
    @param s_loc: s location of the place to check wrt nominal SPS files (s = 0 => SPS$START)
    @param element_name: name of the element
    @param aperture: aperture of the element to check (mm)
    @param figure: figure number
    @return: available aperture in mm wrt the envelope and raw separatrix
    """

    if platform.system() == "Linux":
        s, env_p, env_n = pickle.load(
            open(
                "/home/fvelotti/myScripts/Slow_extraction/pickles/separatrix_envelope_nominal.p",
                "rb",
            )
        )
        _, x_p, x_n = pickle.load(
            open(
                "/home/fvelotti/myScripts/Slow_extraction/pickles/separatrix_raw_nominal.p",
                "rb",
            ),
            encoding="latin1",
        )
        aperx = pickle.load(
            open(
                "/home/fvelotti/myScripts/Slow_extraction/pickles/aperture_from_ZS_lss2.p",
                "rb",
            ),
            encoding="latin1",
        )
        sigma_x, names = pickle.load(
            open(
                "/home/fvelotti/myScripts/Slow_extraction/pickles/sigma_x_from_ZS_lss2.p",
                "rb",
            ),
            encoding="latin1",
        )
    else:
        s, env_p, env_n = pickle.load(
            open(
                "/Users/fvelotti/myScripts/Slow_extraction/pickles/separatrix_envelope_nominal.p",
                "rb",
            ),
            encoding="latin1",
        )
        _, x_p, x_n = pickle.load(
            open(
                "/Users/fvelotti/myScripts/Slow_extraction/pickles/separatrix_raw_nominal.p",
                "rb",
            ),
            encoding="latin1",
        )
        aperx = pickle.load(
            open(
                "/Users/fvelotti/myScripts/Slow_extraction/pickles/aperture_from_ZS_lss2.p",
                "rb",
            ),
            encoding="latin1",
        )
        sigma_x, names = pickle.load(
            open(
                "/Users/fvelotti/myScripts/Slow_extraction/pickles/sigma_x_from_ZS_lss2.p",
                "rb",
            ),
            encoding="latin1",
        )

    index_start = names.index('"SPS$START"')

    # s0 = s[index_start]
    # s_f = s[-1]
    # s_new = np.array(cycle_list(s, index_start)) - s0
    #
    # for i in xrange(len(s_new)):
    #     if s_new[i] < 0:
    #         s_new[i] += s_f

    se = {
        "s": np.array(cycle_s(s, index_start)),
        "aper_1": np.array(cycle_list(aperx, index_start)),
        "sigma": np.array(cycle_list(sigma_x, index_start)),
        "x_pos": np.array(cycle_list(x_p, index_start)),
        "x_neg": np.array(cycle_list(x_n, index_start)),
        "env_pos": np.array(cycle_list(env_p, index_start)),
        "env_neg": np.array(cycle_list(env_n, index_start)),
    }

    return se


def plot_aper(s, aper, unit="m"):
    if not unit == "mm":
        plt.fill_between(s, aper, 0.2, color="black", alpha=0.4)
        plt.fill_between(s, -1 * aper, -0.2, color="black", alpha=0.4)
        plt.plot(s, aper, "k")
        plt.plot(s, -1 * aper, "k")
    else:
        plt.fill_between(s, aper, 0.2 * 1e3, color="black", alpha=0.4)
        plt.fill_between(s, -1 * aper, -0.2 * 1e3, color="black", alpha=0.4)
        plt.plot(s, aper, "k")
        plt.plot(s, -1 * aper, "k")


def plot_aper_asymmetric(s, aper, aper_n, unit="m"):
    if not unit == "mm":
        plt.fill_between(s, aper, 0.2, color="black", alpha=0.4)
        plt.fill_between(s, aper_n, -0.2, color="black", alpha=0.4)
        plt.plot(s, aper, "k")
        plt.plot(s, aper_n, "k")
    else:
        plt.fill_between(s, aper, 0.2 * 1e3, color="black", alpha=0.4)
        plt.fill_between(s, aper_n, -0.2 * 1e3, color="black", alpha=0.4)
        plt.plot(s, aper, "k")
        plt.plot(s, aper_n, "k")


def get_blm_TL(path_files, plane, phase, tl):
    """
    Get the recorded blm reading all along the transfer line
    @param path_files: path where to find the specific files (see code below for names)
    @param plane: 'hor' or 'ver'
    @param phase: oscillation phase
    @param tl: ti2 or ti8
    @return: losses normalised
    """

    # get intensity of each shot
    with open(path_files + "intensity_oscillations.txt", "r") as fi:
        fi.next()
        for line in fi:
            col = line.split()
            if float(col[0]) == phase:
                if plane == "hor":
                    int_shot = float(col[1])
                elif plane == "ver":
                    int_shot = float(col[2])

    # get normalisation values for collimator BLMs

    blm_coll = []
    calibration = []

    with open(path_files + "calibration.txt", "r") as fc:
        fc.next()
        for line in fc:
            col = line.split()
            blm_coll.append(col[0])
            if 30 <= phase <= 180:
                calibration.append(float(col[2]))
            else:
                calibration.append(float(col[1]))

    blm_names = []
    blm_losses = []
    with open(path_files + tl + "_" + plane + "_blm_data.txt", "r") as fd:
        phase_index = fd.readline().split().index(str(phase))
        for line in fd:
            col = line.split()
            blm_names.append(col[1])
            blm_losses.append(float(col[phase_index]))

    l_loc = np.array(blm_losses) / int_shot

    # max of losses recorded during oscillations
    l_5s = calibration[blm_coll.index(blm_names[blm_losses.index(max(blm_losses))])]

    return l_loc / l_5s


def get_blm_coll(path_files, tl, plane, phase, amplitude):
    """
    Obtain the blm readings at the location of the TL collimators only.
    @param path_files: path where the data files are stored
    @param tl: ti2 or ti8
    @param plane: hor or ver
    @param phase: oscillation phase
    @param s:
    @return:
    """

    blm_s = []

    with open(
        "/afs/cern.ch/work/f/fvelotti/public/" + tl.upper() + "/blm_pos.txt", "r"
    ) as fb:
        for line in fb:
            column = line.split()
            if tl == "ti2":
                blm_s.append(float(column[1]) + 241.391155)
            else:
                blm_s.append(float(column[1]) + 156.2065)
    # get intensity of each shot
    with open(path_files + "intensity_oscillations.txt", "r") as fi:
        fi.next()
        for line in fi:
            col = line.split()
            if float(col[0]) == phase:
                if plane == "hor":
                    int_shot = float(col[1])
                elif plane == "ver":
                    int_shot = float(col[2])

    # get normalisation values for collimator BLMs

    blm_coll = []
    calibration_left = []
    calibration_right = []

    with open(path_files + "calibration.txt", "r") as fc:
        fc.next()
        for line in fc:
            col = line.split()
            blm_coll.append(col[0])
            calibration_left.append(float(col[1]))
            calibration_right.append(float(col[2]))

    blm_names = []
    blm_losses = []
    with open(path_files + tl + "_" + plane + "_blm_data.txt", "r") as fd:
        phase_index = fd.readline().split().index(str(phase))
        for line in fd:
            col = line.split()
            blm_names.append(col[1])
            blm_losses.append(float(col[phase_index]))

    # Check which jaw has been hit - left or right

    is_left = []

    x_col = []
    y_col = []

    betax_col = []
    betay_col = []

    dx_col = []
    dy_col = []

    namest, xt, betxt, dxt, yt, betyt, dyt = get_twiss_numpy(
        "/afs/cern.ch/work/f/fvelotti/public/"
        + tl.upper()
        + "/oscillations_twiss/twiss_"
        + plane
        + "_mu_"
        + str(phase)
        + "_"
        + str(amplitude)
        + ".tfs",
        "",
        ["name", "x", "betx", "dx", "y", "bety", "dy"],
    )

    for i, name in enumerate(namest):

        if (re.search('"TCDIH', name) or re.search('"TCDIV', name)) and not re.search(
            '"TCDIV\.20607"', name
        ):

            if name[5] == "H":
                print(name)
                if float(xt[i]) > 0.0:
                    is_left.append(True)
                    x_col.append(xt[i - 1])
                    betax_col.append(betxt[i - 1])
                    dx_col.append(dxt[i - 1])

                    y_col.append(yt[i - 1])
                    betay_col.append(betyt[i - 1])
                    dy_col.append(dyt[i - 1])
                else:
                    is_left.append(False)
                    x_col.append(xt[i - 1])
                    betax_col.append(betxt[i - 1])
                    dx_col.append(dxt[i - 1])

                    y_col.append(yt[i - 1])
                    betay_col.append(betyt[i - 1])
                    dy_col.append(dyt[i - 1])
            elif name[5] == "V":
                print(name)
                if float(yt[i]) > 0.0:
                    is_left.append(True)
                    x_col.append(xt[i - 1])
                    betax_col.append(betxt[i - 1])
                    dx_col.append(dxt[i - 1])

                    y_col.append(yt[i - 1])
                    betay_col.append(betyt[i - 1])
                    dy_col.append(dyt[i - 1])
                else:
                    is_left.append(False)
                    x_col.append(xt[i - 1])
                    betax_col.append(betxt[i - 1])
                    dx_col.append(dxt[i - 1])

                    y_col.append(yt[i - 1])
                    betay_col.append(betyt[i - 1])
                    dy_col.append(dyt[i - 1])
    print(is_left)
    actual_blm = []
    actual_s = []
    unormalised_losses = []

    for name in blm_coll:
        index = blm_names.index(name)
        actual_s.append(blm_s[index])

        actual_blm.append(blm_losses[index] / int_shot)

    for i in range(len(is_left)):
        if is_left[i]:
            unormalised_losses.append(actual_blm[i] / calibration_left[i])
        else:
            unormalised_losses.append(actual_blm[i] / calibration_right[i])

    # unormalised_losses = np.array(actual_blm) / calibration

    if plane == "hor":
        return actual_s, unormalised_losses, x_col, betax_col, dx_col
    else:

        return actual_s, unormalised_losses, y_col, betay_col, dy_col


def get_twiss_dic(file_name, namesConv=False):

    """
    Needs a twiss file as input and gives 2 dictionaries back.

    @param file_name: twiss file name
    @return: dict of twiss columns (data) and dict of twiss summary table (beam_info)
    """

    beam_info = {}

    with open(file_name, "r") as ff:
        count = 0
        for i in range(1, 50):
            line = ff.readline().split()
            if line[0] == "@":
                count += 1
                try:
                    beam_info[line[1]] = float(line[3])
                except:
                    beam_info[line[1]] = line[3]
            elif line[0] == "*":
                names = line[1:]

    data = np.genfromtxt(file_name, skip_header=count + 2, names=names, dtype=None)
    if "NAME" in names:
        namesAll = list(data["NAME"])
        namesAll = [ele.decode("utf-8") for ele in namesAll]

    if not namesConv:
        return data, beam_info
    else:
        return data, beam_info, namesAll


def read_tfs(file_name):
    """
    Read any tfs table generated from MADX

    @param file_name: twiss file name
    @return: dic of the twiss file and dic of the info of the table
    """

    info = {}

    with open(file_name, "r") as ff:
        for i, line in enumerate(ff):
            eles = line.split()

            if eles[0] == "*":
                names = line.split()
                del names[0]
                print(names)
            elif eles[0] == "$":
                head = i + 1
                break
            else:
                try:
                    info[eles[1]] = float(line[3])
                except:
                    info[line[1]] = line[3]

    size = len(linecache.getline(file_name, head + 1).split())
    todel = ["todel"] * (size - len(names))
    names_temp = names + todel
    data = np.genfromtxt(
        file_name, skip_header=head, names=names_temp, dtype=None, invalid_raise=False
    )

    data = data[names]

    return data, info


def calc_acceptance(s_list, x_list, sigma_list, aper_list):
    """
    Calcualte acceptance given the above vectors
    :param s_list: twiss s
    :param x_list: twiss x or zeros
    :param sigma_list: sigma(s)
    :param aper_list: aper twiss
    :return: numpy array of new s and accetance
    """

    acc = []
    s_acc = []

    for i in range(len(aper_list)):
        if aper_list[i] != 0:
            s_acc.append(s_list[i])
            acc.append((aper_list[i] - np.abs(x_list[i])) / sigma_list[i])

    return np.array(s_acc), np.array(acc)


def calc_element_acceptance(twiss, element, plane):
    """
    Calcualte acceptance of a given element for injected FT beam
    :param twiss: dictionary of a twiss file for injected FT beam
    :return: numpy array of new s and accetance
    """

    if plane == "hor":
        for i in range(len(twiss["APER_1"])):
            if twiss["NAME"][i].strip('"') == element:
                acc = (twiss["APER_1"][i]) / np.sqrt(
                    twiss["BETX"][i] * 12e-6 / 14.955 + twiss["DX"][i] ** 2 * 1e-6
                )
    else:

        for i in range(len(twiss["APER_2"])):
            if twiss["NAME"][i].strip('"') == element:
                acc = (twiss["APER_2"][i]) / np.sqrt(
                    twiss["BETY"][i] * 7.5e-6 / 14.955 + twiss["DY"][i] ** 2 * 1e-6
                )

    return acc


def blf(
    s1,
    s2,
    aper_x,
    aper_y,
    twiss,
    beam,
    target_x="none",
    target_y="none",
    separatrix_type="env",
):
    """
    Calculates the acceptances for FT injected and extracted for a given s location [s1, s2] and for a given
    aperture (x ,y).
    @param s1:
    @param s2:
    @param aper_x: half aperture
    @param aper_y: half aperture
    @param twiss: dictionary
    @param beam: dictionary
    @param target_x:
    @param target_y:
    @param separatrix_type: 'env' (envelope with tolerances) or 'x' (only raw sepratrix)
    @return: dictionary with the different acceptances
    """
    s_to_check = np.linspace(s1, s2, 50)

    # Slow extracted beam envelope
    separatrix = get_separatrix()

    s_sep = separatrix["s"]
    x_sep_pos = np.interp(s_to_check, s_sep, separatrix[separatrix_type + "_pos"])
    x_sep_neg = np.interp(s_to_check, s_sep, separatrix[separatrix_type + "_neg"])
    sep_sigma = np.interp(s_to_check, s_sep, separatrix["sigma"])

    acc_sep = aper_x - np.array(
        [max(x_sep_pos[i], abs(x_sep_neg[i])) for i in range(len(x_sep_pos))]
    )

    target_x_sep = 1

    if target_x == "none":
        target_x_value = 1
        target_x_sep = sep_sigma
    elif type(target_x) == str:
        target_x_value = calc_element_acceptance(twiss, target_x, "hor")
        if separatrix_type == "env":
            target_x_sep, _ = get_se_apertures(target_x)
        else:
            _, target_x_sep = get_se_apertures(target_x)

    if target_y == "none":
        target_y = 1
    elif type(target_y) == str:
        target_y = calc_element_acceptance(twiss, target_y, "ver")

    print("target hor: ", target_x_value)
    print("target sep: ", target_x_sep)
    print("target ver: ", target_y)

    # Separatrix in mm
    acc_sep_mm = acc_sep * 1e3
    # Separatrix
    acc_sep = acc_sep / target_x_sep

    # Horizontal
    betx = np.interp(s_to_check, twiss["S"], twiss["BETX"])
    # x = np.interp(s_to_check, twiss['S'], twiss['X'])
    dx = np.interp(s_to_check, twiss["S"], twiss["DX"])
    sigma_h = np.sqrt(betx * 1.2 * 12e-6 / (beam["GAMMA"]) + (dx * 1.5e-3) ** 2)
    acc_h = aper_x / sigma_h / target_x_value

    # Vertical
    bety = np.interp(s_to_check, twiss["S"], twiss["BETY"])
    # y = np.interp(s_to_check, twiss['S'], twiss['Y'])
    dy = np.interp(s_to_check, twiss["S"], twiss["DY"])
    sigma_v = np.sqrt(bety * 1.2 * 7.5e-6 / (beam["GAMMA"]) + (dy * 1.5e-3) ** 2)
    acc_v = aper_y / sigma_v / target_y

    best_location = {
        "s": s_to_check,
        "a_x": acc_h,
        "a_y": acc_v,
        "a_s": acc_sep,
        "a_s_m": acc_sep_mm,
    }

    return best_location


def normalise(u, pu, beta, alfa):
    u_norm = u / np.sqrt(beta)
    pu_norm = (u * alfa + beta * pu) / np.sqrt(beta)

    return u_norm, pu_norm


def plot_aper_as_blocks(s, aperx, l):
    for n in range(len(aperx)):
        if aperx[n] == 0:
            aperx[n] = 1

    for n in range(1, len(s)):

        temp_x = s[n] - l[n]
        temp_y = aperx[n]
        xy = temp_x, temp_y
        xy_2 = temp_x, -1
        width = l[n]
        heigth_pos = 1 - aperx[n]
        heigth = 1 - aperx[n]
        p = patches.Rectangle(xy, width, heigth_pos, fc="k", alpha=0.4, edgecolor="k")
        p_2 = patches.Rectangle(xy_2, width, heigth, fc="k", alpha=0.4, edgecolor="k")
        plt.gca().add_patch(p)
        plt.gca().add_patch(p_2)


def filled_hist(data, *hist_option, **kwargs):
    if not "alpha" in kwargs:
        kwargs["alpha"] = 0.15

    _, _, hist = plt.hist(data, histtype="stepfilled", lw=0, *hist_option, **kwargs)
    kwargs.pop("alpha")
    if "label" in kwargs:
        kwargs.pop("label")
    if "color" in kwargs:
        kwargs.pop("color")

    plt.hist(
        data,
        histtype="step",
        color=hist[0].get_facecolor(),
        alpha=1,
        *hist_option,
        **kwargs,
    )

    # if fitted:
    #     mu = np.mean(data)
    #     sigma = np.std(data)
    #     pdf_x_x = np.linspace(-5 * sigma, 5 * sigma, 1000)
    #     pdf_x = gauss.pdf(pdf_x_x, loc=mu, scale=sigma)
    #     plt.plot(pdf_x_x, pdf_x, lw=1, color=hist[0].get_facecolor())


def stem(x, y, **kwargs):

    if not "color" in kwargs:
        (color_def,) = plt.plot([], [])
        col = color_def.get_color()
    else:
        col = kwargs["color"]
        kwargs.pop("color")

    (_marker, _line, _base) = plt.stem(x, y, basefmt=col, **kwargs)

    kwargs.pop("label")

    l = plt.setp(_marker, color=col, markeredgecolor=col, **kwargs)
    l = plt.setp(_line, color=col, **kwargs)


def freedman_diacoins_bins(x):
    iqr = np.subtract(*np.percentile(x, [75, 25]))
    h = 2 * iqr / len(x) ** (1 / 3.0)
    return np.round((max(x) - min(x)) / h)


def plot_dist_fit(x, dist):

    fit_x = dist.fit(x)
    x_fit = np.linspace(0.8 * min(x), 1.2 * max(x), 300)
    plt.plot(x_fit, dist.pdf(x_fit, *fit_x))


def plot_hist_fit(data, dist, label=None, bins_minus=0, ls=".", bins=0):

    if bins == 0:
        bins_n = freedman_diacoins_bins(data) - bins_minus
    else:
        bins_n = bins
    count, bins = get_hist_vec(data, bins=bins_n, density=True)
    (mark,) = plt.plot(bins, count, ls)

    fit = dist.fit(data)

    min_data = min(data)
    max_data = max(data)

    x_fit = np.linspace(
        min_data - abs(min_data * 0.2), max_data + abs(max_data * 0.2), 300
    )

    plt.plot(x_fit, dist.pdf(x_fit, *fit), color=mark.get_color(), label=label)


def make_SPS_knob(path, madx_file, knob_file, nominal):
    names = []
    strengths = []

    with open(path + madx_file, "r") as fr:
        for line in fr:
            col = line.split()
            names.append(col[0])
            strengths.append(float(col[2]))

    with open(path + knob_file + ".tfs", "wb") as fw:

        fw.write(
            '@TITLE  "' + knob_file + '" \n'
            '@ NAME %05s  "TWISS" \n'
            '@ TIME %08s  "12.40.00 \n'
            "* NAME KICK \n"
            "$ %s       %le\n"
        )
        for name_strength in zip(names, strengths):
            fw.write(
                '"logical.'
                + name_strength[0][1:].upper()
                + '/K"'
                + "    "
                + str(name_strength[1] / nominal)
                + "\n"
            )


def get_bsg23_data(file_name, plot_on=False):

    data = np.genfromtxt(file_name, delimiter=",", skip_header=1)[:, -5:]

    hor3 = data[:15, :]
    ver3 = data[17:, :]

    hor_vec = hor3[::-1, 1]
    ver_vec = ver3[::-1, 1]

    x = np.linspace(-6, 6, 15)

    if plot_on:
        plt.figure(1)
        plt.plot(x, hor_vec, "o")
        plt.figure(2)
        plt.plot(x, ver_vec, "o")

    # hor_vec = np.append(hor_vec, 0)
    # hor_vec = np.insert(hor_vec, 0, 0)
    #
    # ver_vec = np.append(ver_vec, 0)
    # ver_vec = np.insert(ver_vec, 0, 0)
    #
    # x_new = np.append(x, 16)
    # x_new = np.insert(x_new, 0, -16)
    # x_fit = np.linspace(-16, 16, 50)
    #
    # hor_inter = np.interp(x_fit, x_new, hor_vec)
    # ver_inter = np.interp(x_fit, x_new, ver_vec)
    #
    # if plot_on:
    #     plt.figure(1)
    #     plt.plot(x_fit, hor_inter)
    #     plt.figure(2)
    #     plt.plot(x_fit, ver_inter)

    # return x_fit, hor_inter, ver_inter
    return x, hor_vec, ver_vec


def get_bsg156_data(file_name, plot_on=False):

    data = np.genfromtxt(file_name, delimiter=",", skip_header=1)[:, -5:]

    hor3 = data[:15, :]
    ver3 = data[17:, :]

    hor_vec = hor3[:, 1]
    ver_vec = ver3[::-1, 1]

    x = np.linspace(-16, 16, 15)

    if plot_on:
        plt.figure(1)
        plt.plot(x, hor_vec, "o")
        plt.figure(2)
        plt.plot(x, ver_vec, "o")

    # hor_vec = np.append(hor_vec, 0)
    # hor_vec = np.insert(hor_vec, 0, 0)
    #
    # ver_vec = np.append(ver_vec, 0)
    # ver_vec = np.insert(ver_vec, 0, 0)
    #
    #
    # x_new = np.append(x, 16)
    # x_new = np.insert(x_new, 0, -16)
    # x_fit = np.linspace(-16, 16, 50)
    #
    # hor_inter = np.interp(x_fit, x_new, hor_vec)
    # ver_inter = np.interp(x_fit, x_new, ver_vec)
    #
    # if plot_on:
    #     plt.figure(1)
    #     plt.plot(x_fit, hor_inter)
    #     plt.figure(2)
    #     plt.plot(x_fit, ver_inter)
    #
    # return x_fit, hor_inter, ver_inter
    return x, hor_vec, ver_vec


def scatter_plot(x, y):

    xy = np.vstack([x, y])
    z = gaussian_kde(xy)(xy)
    plt.scatter(x, y, c=z, s=10, edgecolor="", cmap="viridis")


def pdf_double_gauss(x, loc=0.0, scale=1.0, beam=1):

    if beam == 2:
        # Beam 2
        sigma1, c2, sigma2 = 1.28147532, 0.6013564, 0.38372913
    elif beam == 1:
        # Beam 1
        sigma1, c2, sigma2 = 0.8684419, 0.21909869, 0.33253133

    sigma1 *= np.sqrt(7.3e-9 * 42.7)
    sigma2 *= np.sqrt(7.3e-9 * 42.7)

    nom_sig = np.sqrt(1e-6 / (3.5e-6 / 7.3e-9) * 42.7)

    sc1 = sigma1 / nom_sig
    sc2 = sigma2 / nom_sig

    s1 = sc1 * scale
    s2 = sc2 * scale

    res = (1 - c2) * norm.pdf(x, loc=loc, scale=s1) + c2 * norm.pdf(
        x, loc=loc, scale=s2
    )

    return res


def rvs_double_gauss(n, mu=0, sigma=1.0, beam=1):

    if beam == 2:
        # Beam 2
        sigma1, c2, sigma2 = 1.28147532, 0.6013564, 0.38372913
    elif beam == 1:
        # Beam 1
        sigma1, c2, sigma2 = 0.8684419, 0.21909869, 0.33253133

    sigma1 *= np.sqrt(7.3e-9 * 42.7)
    sigma2 *= np.sqrt(7.3e-9 * 42.7)

    nom_sig = np.sqrt(1e-6 / (3.5e-6 / 7.3e-9) * 42.7)

    sc1 = sigma1 / nom_sig
    sc2 = sigma2 / nom_sig

    s1 = sc1 * sigma
    s2 = sc2 * sigma

    tot_len = int(n * (1 - c2)) + int(n * c2)

    if not tot_len == n:
        add_one = 1
    else:
        add_one = 0

    res1 = norm(loc=mu, scale=s1).rvs(int(n * (1 - c2)))

    res2 = norm(loc=mu, scale=s2).rvs(int(n * c2) + add_one)

    return np.r_[res1, res2]


def collect_losses_per_turn(file_name):
    losses = {}
    with open(file_name, "r") as fr:
        for _ in range(0, 8):
            next(fr)
        for line in fr:
            col = line.split()
            turn = col[1]

            if int(turn) not in losses.keys():
                losses[int(turn)] = []

            losses[int(turn)].append(float(col[8]))

    return losses


def collect_losses_per_name(file_name):
    losses = {}
    with open(file_name, "r") as fr:
        for _ in range(0, 8):
            next(fr)
        for line in fr:
            col = line.split()
            name = col[10][1:]
            print(name)

            if name not in losses.keys():
                losses[name] = []

            losses[name].append(float(col[8]))

    return losses


def collect_losses(file_name):
    losses = {}
    los_file, _ = read_tfs(file_name)
    elements = [ele.strip('"') for ele in los_file["ELEMENT"]]

    ele_losses = set(elements)

    for ele in ele_losses:
        mask = np.array(elements) == ele
        losses[ele] = los_file["S"][mask]

    return losses


def legend(pos="top", ncol=2):

    if pos == "top":
        plt.legend(
            bbox_to_anchor=(0.0, 1.02, 1.0, 0.102),
            loc=3,
            ncol=ncol,
            mode="expand",
            borderaxespad=0.0,
        )
    if pos == "right":
        plt.legend(loc="upper left", prop={"size": 6}, bbox_to_anchor=(1, 1), ncol=ncol)


def rvs_double(n, mu1, mu2, s1, s2, c2):

    tot_len = int(n * (1 - c2)) + int(n * c2)

    if not tot_len == n:
        add_one = 1
    else:
        add_one = 0

    res1 = norm(loc=mu1, scale=s1).rvs(int(n * (1 - c2)))

    res2 = norm(loc=mu2, scale=s2).rvs(int(n * c2) + add_one)

    return np.r_[res1, res2]


def rvs_double_one(mu1, mu2, s1, s2, c2):

    choice = np.random.choice([1, 2], p=[1 - c2, c2])

    if choice == 1:
        return norm(loc=mu1, scale=s1).rvs(1)

    else:
        return norm(loc=mu2, scale=s2).rvs(1)


def crystal_pdf(n=1):

    n_tot = {1: 0, 2: 0, 3: 0}

    if n == 1:

        choice = np.random.choice([1, 2, 3], p=[0.4, 0.06, 0.54])
        n_tot[choice] = 1

        if n_tot[1] == 1:
            x1 = rvs_double_one(-14, 0.1, 8.03, 7.3, 0.5)
        else:
            x1 = []

    else:
        n_tot[1] = int(n * 0.4)
        n_tot[2] = int(n * 0.06)
        n_tot[3] = int(n * 0.54)

        if n_tot[1] + n_tot[2] + n_tot[3] < n:
            n_tot[3] += 1
        elif n_tot[3] + n_tot[2] + n_tot[3] > n:
            n_tot[3] -= 1

        x1 = rvs_double(int(n_tot[1]), -14, 0.1, 8.03, 7.3, 0.5)

    s, l = 88.3, 3.0044

    x2 = []
    while len(x2) != n_tot[2]:
        temp = float(expon.rvs(size=1, loc=l, scale=s))
        if temp < 130:
            x2.append(temp)

    x3 = norm.rvs(size=n_tot[3], loc=143.8, scale=8.2)

    return np.r_[x1, x2, x3]


def double_gauss(x, loc1, loc2, s1, s2, c2):
    return (1 - c2) * norm.pdf(x, loc=loc1, scale=s1) + c2 * norm.pdf(
        x, loc=loc2, scale=s2
    )


def crystal_pdf_analytic():

    x1 = np.linspace(-50, 8, 400)
    pdf1 = 0.4 * double_gauss(x1, -14, 0.1, 8.03, 7.3, 0.5)

    s, l = 88.3, 3.0044

    x2 = np.linspace(10, 120, 300)
    pdf21 = 0.06 * expon.pdf(x2, loc=l, scale=s)
    pdf22 = 0.4 * double_gauss(x2, -14, 0.1, 8.03, 7.3, 0.5)
    pdf23 = 0.54 * norm.pdf(x2, loc=143.8, scale=8.2)
    pdf2 = pdf21 + pdf22 + pdf23

    x3 = np.linspace(122, 200, 400)
    pdf3 = 0.54 * norm.pdf(x3, loc=143.8, scale=8.2)

    return np.r_[x1, x2, x3], np.r_[pdf1, pdf2, pdf3]


def remove_eles(alist, indices):
    return [i for j, i in enumerate(alist) if j not in indices]


def find_orbit_fit(ref_file, meas_file, model="", fit="all", max_diff_sigma=2.5):

    co_ref_x = []
    co_ref_y = []

    bpm_h_list = []
    bpm_v_list = []

    data = np.genfromtxt(
        ref_file, names=True, comments="*", dtype=None, skip_header=28, skip_footer=218
    )

    # bad_bpms_list = ['BPCE.', 'BPH.12208', 'BPH.30208', 'BPH.32408', 'BPH.41008', 'BPH.50808']
    bad_bpms_list = ["BPCE.", "BPCN.12508", "BPA.21805", "BPV.30308"]

    bad_bpms = [r"%s" % re.escape(elements.strip()) for elements in bad_bpms_list]
    pattern_bad_bpms = re.compile("|".join(bad_bpms))

    for j in range(len(data["STATUS"])):
        if (
            data["STATUS"][j] == 0
            and data["PLANE"][j] == "H"
            and not pattern_bad_bpms.search(data["NAME"][j])
        ):
            co_ref_x.append(data["POS"][j])
            bpm_h_list.append(data["NAME"][j])

        if (
            data["STATUS"][j] == 0
            and data["PLANE"][j] == "V"
            and not pattern_bad_bpms.search(data["NAME"][j])
        ):
            co_ref_y.append(data["POS"][j])
            bpm_v_list.append(data["NAME"][j])

    co_ref_x = np.array(co_ref_x)
    co_ref_y = np.array(co_ref_y)

    variables = ["name", "s", "l", "x", "betx", "dx", "bety", "y", "dy", "mux", "muy"]

    if model == "":
        path_twiss = "/afs/cern.ch/work/f/fvelotti/private/sps_op/"
        twiss_name = "model_misa_highChroma.tfs"
        name, s, l, x, betx, dx, bety, y, dy, mux, muy = get_twiss_var(
            twiss_name, path_twiss, variables
        )
    else:
        name, s, l, x, betx, dx, bety, y, dy, mux, muy = get_twiss_var(
            model, "", variables
        )

    all_bpms = []
    for i, ele in enumerate(name):
        if "BP" in ele:
            all_bpms.append(ele)

    print("file under analysis: ", meas_file)

    x = []
    y = []
    bpm_temp_x = []
    bpm_temp_y = []
    co_dif_x = []
    co_dif_y = []

    data = np.genfromtxt(
        meas_file,
        names=True,
        comments="*",
        dtype=None,
        skip_header=28,
        skip_footer=218,
        invalid_raise=False,
    )

    for j in range(len(data["STATUS"])):
        if (
            data["STATUS"][j] == 0
            and data["PLANE"][j] == "H"
            and not pattern_bad_bpms.search(data["NAME"][j])
        ):
            x.append(data["POS"][j])
            bpm_temp_x.append(data["NAME"][j])

        if (
            data["STATUS"][j] == 0
            and data["PLANE"][j] == "V"
            and not pattern_bad_bpms.search(data["NAME"][j])
        ):
            y.append(data["POS"][j])
            bpm_temp_y.append(data["NAME"][j])

    for i, bpm in enumerate(bpm_temp_x):
        co_dif_x.append(x[i] - co_ref_x[bpm_h_list.index(bpm)])

    for i, bpm in enumerate(bpm_temp_y):
        co_dif_y.append(y[i] - co_ref_y[bpm_v_list.index(bpm)])

    sigma = np.std(co_dif_x)
    sigma_y = np.std(co_dif_y)

    bed_bpms_indeces = []
    for j in range(len(co_dif_x)):
        if abs(co_dif_x[j]) > max_diff_sigma * sigma:
            bed_bpms_indeces.append(j)

    co_dif_x = remove_eles(co_dif_x, bed_bpms_indeces)
    bpm_temp_x = remove_eles(bpm_temp_x, bed_bpms_indeces)

    bed_bpms_indeces_y = []
    for j in range(len(co_dif_y)):
        if abs(co_dif_y[j]) > max_diff_sigma * sigma_y:
            bed_bpms_indeces_y.append(j)

    co_dif_y = remove_eles(co_dif_y, bed_bpms_indeces_y)
    bpm_temp_y = remove_eles(bpm_temp_y, bed_bpms_indeces_y)

    betx_bpm = np.array(get_bpm_data(name, betx, bpm_temp_x))
    dx_bpm = np.array(get_bpm_data(name, dx, bpm_temp_x))
    mux_bpm = np.array(get_bpm_data(name, mux, bpm_temp_x))
    s_bpm = np.array(get_bpm_data(name, s, bpm_temp_x))

    betx = np.array(betx)
    mux = np.array(mux)
    dx = np.array(dx)
    s = np.array(s)

    bety_bpm = np.array(get_bpm_data(name, bety, bpm_temp_y))
    dy_bpm = np.array(get_bpm_data(name, dy, bpm_temp_y))
    muy_bpm = np.array(get_bpm_data(name, muy, bpm_temp_y))

    bety = np.array(bety)
    muy = np.array(muy)
    dy = np.array(dy)

    co_dif_x = 1e-6 * np.array(co_dif_x) / np.sqrt(betx_bpm)
    co_dif_y = 1e-6 * np.array(co_dif_y) / np.sqrt(bety_bpm)

    def orbit_all(mux_var, a, b, dp):
        return (
            a * np.sin(2 * np.pi * mux_var) + b * np.cos(2 * np.pi * mux_var)
        ) + dp * dx / np.sqrt(betx)

    def orbit_all_y(muy_var, a, b, dp):
        return (
            a * np.sin(2 * np.pi * muy_var) + b * np.cos(2 * np.pi * muy_var)
        ) + dp * dy / np.sqrt(bety)

    def orbit(mux_in, a, b, dp):
        return (
            a * np.sin(2 * np.pi * mux_in) + b * np.cos(2 * np.pi * mux_in)
        ) + dp * dx_bpm / np.sqrt(betx_bpm)

    def orbit_y(muy_in, a, b, dp):
        return (
            a * np.sin(2 * np.pi * muy_in) + b * np.cos(2 * np.pi * muy_in)
        ) + dp * dy_bpm / np.sqrt(bety_bpm)

    def orbit_bpce(mux_var, a, b, dp):
        return (
            a * np.sin(2 * np.pi * mux_var) + b * np.cos(2 * np.pi * mux_var)
        ) + dp * dx_bpce / np.sqrt(betx_bpce)

    def orbit_bpce_y(muy_var, a, b, dp):
        return (
            a * np.sin(2 * np.pi * muy_var) + b * np.cos(2 * np.pi * muy_var)
        ) + dp * dy_bpce / np.sqrt(bety_bpce)

    # Fit horizontal plane
    popt, pcov = curve_fit(orbit, mux_bpm, co_dif_x)

    # Fit vertical plane
    popt_y, pcov_y = curve_fit(orbit_y, muy_bpm, co_dif_y)

    if fit == "bpces":
        bpces = ["VVSB.41757", "BPCE.41801", "MDHB.61804", "BPCE.61805"]

        betx_bpce = get_names_data(name, betx, bpces)
        mux_bpce = get_names_data(name, mux, bpces)
        dx_bpce = get_names_data(name, dx, bpces)
        s_bpce = get_names_data(name, s, bpces)

        bety_bpce = get_names_data(name, bety, bpces)
        muy_bpce = get_names_data(name, muy, bpces)
        dy_bpce = get_names_data(name, dy, bpces)

        x_bpces = np.array(orbit_bpce(mux_bpce, *popt)) * np.sqrt(betx_bpce)
        y_bpces = np.array(orbit_bpce_y(muy_bpce, *popt_y)) * np.sqrt(bety_bpce)

        return s_bpce, (x_bpces, y_bpces), (popt, popt_y), bpces

    elif fit == "all":

        x_all = orbit_all(mux, *popt) * np.sqrt(betx)
        y_all = orbit_all_y(muy, *popt_y) * np.sqrt(bety)

        return (
            s,
            (x_all, y_all),
            (popt, popt_y),
            (s_bpm, orbit(mux_bpm, *popt) * np.sqrt(betx_bpm), bpm_temp_x, bpm_temp_y),
        )


def get_file(path, string_to_match):
    """
    Get the actual file name starting from a partial name
    """
    files = os.listdir(path)

    for file in files:
        if string_to_match in file:
            file_reference = file

    return file_reference


def get_files(path, string_to_match):
    """
    Get the files that matches a certain string
    """
    files = os.listdir(path)
    match_files = []

    for file in files:
        if string_to_match in file:
            match_files.append(file)

    return match_files


def sdds_to_dict(in_complete_path):

    try:
        temp = sddsdata(in_complete_path, endian="little", full=True)
    except IndexError:
        print("Failed to open data file. (save_multiq_mat)")
        return
    return temp.data[0]


def errorbar(
    x,
    y,
    yerr=None,
    xerr=None,
    fmt="o",
    ecolor=None,
    elinewidth=0.5,
    capsize=None,
    barsabove=False,
    lolims=False,
    uplims=False,
    xlolims=False,
    xuplims=False,
    errorevery=1,
    capthick=None,
    mfc="w",
    **kwargs,
):

    return plt.errorbar(
        x,
        y,
        yerr=yerr,
        xerr=xerr,
        fmt=fmt,
        ecolor=ecolor,
        elinewidth=elinewidth,
        capsize=capsize,
        barsabove=barsabove,
        lolims=lolims,
        uplims=uplims,
        xlolims=xlolims,
        xuplims=xuplims,
        errorevery=errorevery,
        capthick=capthick,
        mfc=mfc,
        **kwargs,
    )


def plot_error(x, y, err, label=""):
    (line,) = plt.plot(x, y, label=label)
    plt.fill_between(x, y + err, y - err, color=line.get_color(), alpha=0.3)


def plot_errorfill(x, y, yerr, color=None, alpha_fill=0.2, **kwargs):

    if np.isscalar(yerr) or len(yerr) == len(y):
        ymin = np.array(y) - np.array(yerr)
        ymax = np.array(y) + np.array(yerr)
    elif len(yerr) == 2:
        ymin, ymax = yerr
    if color == None:
        p0 = plt.plot(x, y, **kwargs)
    else:
        plt.plot(x, y, color=color, **kwargs)
    plt.fill_between(x, ymax, ymin, color=p0[0].get_color(), alpha=alpha_fill)
    return p0


def get_diff(x1, y1, x2, y2, kind="slinear", samples=200):

    x_min = max(x1[0], x2[0])
    x_max = min(x1[-1], x2[-1])

    x_new = np.linspace(x_min, x_max, samples)

    f1 = interp1d(x1, y1, kind=kind)
    f2 = interp1d(x2, y2, kind=kind)

    return x_new, f1(x_new) - f2(x_new)


def plot_name(twiss, name_value, name_list, xpos_offset, ypos, color):
    plt.text(
        twiss["S"][name_list.index(name_value) + xpos_offset],
        ypos,
        name_value.strip('"'),
        fontsize=7,
        color=color,
    )


def gauss_param(x, cen, wid, amp, slope, intercept):

    gauss = (amp / (np.sqrt(2 * np.pi) * wid)) * np.exp(
        -((x - cen) ** 2) / (2 * wid ** 2)
    )
    line = slope * x + intercept
    return gauss + line

    # return (amp / (np.sqrt(2 * np.pi) * wid)) * np.exp(-(x - cen)**2 / (2 * wid**2)) + (m * x) + n


def double_gauss_param(x, cen, wid, amp, cen2, wid2, amp2):

    return (
        (amp / (np.sqrt(2 * np.pi) * wid))
        * np.exp(
            -((x - cen) ** 2) / (2 * wid ** 2) + amp2 / (np.sqrt(2 * np.pi) * wid2)
        )
        * np.exp(-((x - cen2) ** 2) / (2 * wid2 ** 2))
    )


def make_gauss_fit(x, y, p0=[1, 1, 1, 1, 1]):
    """
    Make gaussian fit with least square minimisation
    p0 is ddefined as [mean, std, amplitude]
    """

    return curve_fit(gauss_param, x, y, p0=p0)


def make_doubel_gauss_fit(x, y, p0=[1, 1, 1, 1, 1, 1]):
    """
    Make double gaussian fit with least square minimisation
    p0 is ddefined as [mean1, std1, amplitude1, mean2, std2, amplitude2]
    """
    return curve_fit(double_gauss_param, x, y, p0=p0)


def read_misa_madx_file(file_name):
    """
    Read a misalignment MADX file and return a dictionary of elements with Hor misalignments
    """

    misa_dic = {}
    patt = False
    with open(file_name, "r") as misa_file:
        for line in misa_file:

            line = line.upper()

            if "PATTERN" in line:
                patt = True
                _, _, name_raw = line.partition("PATTERN")
                oneline = []
                for e in name_raw:
                    if e.isalnum() or e == ".":
                        oneline.append(e)
                name = "".join(oneline)
                misa_dic[name] = 0.0
                continue
            if patt:
                _, _, dx = line.partition("=")
                dx = float(dx.translate(None, ";"))
                misa_dic[name] = dx
            patt = False
    return misa_dic


def get_logging_data(variable, *args, **kwargs):
    ld = pytimber.LoggingDB()
    data_all = ld.get(variable, *args, **kwargs)

    ts = [datetime.fromtimestamp(ele) for ele in data_all[variable[0]][0]]

    _, data_all = data_all[variable]

    if len(data_all.shape) != 1:
        test = {variable: [data_all[i, :] for i in range(data_all.shape[0])]}
        df = pd.DataFrame(test, index=ts)
    else:
        df = pd.DataFrame(data_all, columns=[variable], index=ts)

    return df


def unixtime2datetime(x):
    return datetime.fromtimestamp(x)


def unixtime2datetimeVectorize(x):
    """Transform unixtime in python datetime"""
    aux = np.vectorize(unixtime2datetime)
    if np.size(x) != 0:
        return aux(x)
    else:
        return []


def fromTimberToDataFrame(
    listOfVariableToAdd, t1, t2, verbose=False, fundamental="", master=None
):
    """
    Returns a dataframe with a colum cycleStamp (UTC time in ns)
    """
    listOfVariableToAdd = list(set(listOfVariableToAdd))
    ld = pytimber.LoggingDB()
    if fundamental == "":
        DATA = ld.getAligned(listOfVariableToAdd, t1, t2, master=master)
    else:
        DATA = ld.getAligned(listOfVariableToAdd, t1, t2, fundamental, master=master)
    myDataFrame = pd.DataFrame({})
    if DATA != {}:
        for i in listOfVariableToAdd:
            myDataFrame[i] = pd.Series(
                DATA[i].tolist(), unixtime2datetimeVectorize(DATA["timestamps"])
            )
        myDataFrame["cycleStamp"] = np.double(myDataFrame.index.astype(np.int64))
    return myDataFrame


def fromTimberToDataFrameNotAligned(
    listOfVariableToAdd, t1, t2, verbose=False, fundamental=""
):
    listOfVariableToAdd = list(set(listOfVariableToAdd))
    ld = pytimber.LoggingDB()
    if fundamental == "":
        DATA = ld.get(listOfVariableToAdd, t1, t2)
    else:
        DATA = ld.get(listOfVariableToAdd, t1, t2, fundamental)
    myDataFrame = pd.DataFrame({})
    if DATA != {}:
        for i in listOfVariableToAdd:
            myDataFrame[i] = pd.Series(
                DATA[i][1].tolist(), unixtime2datetimeVectorize(DATA[i][0])
            )
        myDataFrame["cycleStamp"] = np.double(myDataFrame.index.astype(np.int64))
    return myDataFrame


def change_shape(x, y, z):
    list_temp = [np.reshape(x, ((int(y) / int(z)), int(z)))]
    return list_temp


def check_saturation(profiles):
    max_adc = 16383.0
    max_bbb = np.max(profiles, axis=1)
    all_sat = [(max_bbb >= max_adc)][0]
    return any(ele for ele in all_sat)


def make_fit(x_in, y, cut=10):
    fit_param = []
    error_fit = []
    x_ws = np.array(x_in)
    mask = (x_ws <= cut) & (x_ws >= -1 * cut)
    for bunch in y:
        mu_start = x_ws[np.argmax(bunch)]

        try:
            opt, cov = make_gauss_fit(
                x_ws[mask], bunch[mask], p0=[mu_start, 1, 1e4, 0, 1]
            )

            error_fit.append(np.sqrt(np.diag(cov)))
            fit_param.append(opt)
        except RuntimeError:
            error_fit.append(np.zeros(5))
            fit_param.append(np.zeros(5))
    #         plt.figure()
    #         plt.plot(x_ws[mask], bunch[mask], 'o')
    #         x_fit = np.linspace(min(x_ws), max(x_ws), 2000)
    #         plt.plot(x_fit, al.gauss_param(x_fit, *opt))

    return fit_param


def make_fit_full(x_in, y, cut=10):
    x_ws = np.array(x_in)
    mask = (x_ws <= cut) & (x_ws >= -1 * cut)
    bunch = np.array(y)
    mu_start = x_ws[np.argmax(bunch)]

    try:
        opt, cov = make_gauss_fit(x_ws[mask], bunch[mask], p0=[mu_start, 1, 1e4, 0, 1])

    except RuntimeError:
        opt = np.zeros(5)

    # plt.figure()
    # plt.plot(x_ws[mask], bunch[mask], 'o')
    # x_fit = np.linspace(min(x_ws), max(x_ws), 2000)
    # plt.plot(x_fit, gauss_param(x_fit, *opt))
    # plt.xlim(-1 * cut, cut)
    return opt.tolist()


def emittance_from_sigma(sigma, momentum, beta):
    gamma = np.sqrt(0.938 ** 2 + momentum ** 2) / 0.938
    beta_r = np.sqrt(1 - 1 / gamma ** 2)
    emit_n = (np.array(sigma) * 1e-3) ** 2 / beta * beta_r * gamma

    return [emit_n * 1e6]


def plot_ws_bbb(df_ws, index_i=0, n_bunches="all"):

    x_fit = np.linspace(
        min(df_ws.x_axis.iloc[index_i]), max(df_ws.x_axis.iloc[index_i]), 2000
    )

    if n_bunches == "all":
        for i in range(df_ws["num_bunches"].iloc[index_i]):
            opt = df_ws["fit"].iloc[index_i][i]

            p = plt.plot(
                df_ws.x_axis.iloc[index_i], df_ws.profile_bbb.iloc[index_i][0][i], "o"
            )

            plt.plot(x_fit, gauss_param(x_fit, *opt), color=p[0].get_color())
    else:
        for i in range(n_bunches[0], n_bunches[1]):
            opt = df_ws["fit"].iloc[index_i][i]

            p = plt.plot(
                df_ws.x_axis.iloc[index_i], df_ws.profile_bbb.iloc[index_i][0][i], "o"
            )

            plt.plot(x_fit, gauss_param(x_fit, *opt), color=p[0].get_color())

    plt.xlabel(r"$\xi$ / mm")
    plt.ylabel("amplitude")


def plot_emit_1d(df_ws, index_i, bunch_range):
    if type(index_i) == int:
        stem(range(bunch_range[0], bunch_range[1]), df_ws["emit_bbb"].iloc[index_i][0])
    else:
        for i in range(index_i[0], index_i[1]):
            if bunch_range == "all":
                bunch_range = [0, df_ws["num_bunches"].iloc[i]]

            stem(
                range(bunch_range[0], bunch_range[1]),
                df_ws["emit_bbb"].iloc[i][bunch_range[0] : bunch_range[1]],
                label=df_ws.index[i],
            )
            plt.legend()


# def plot_emit_3d(df_ws, index_i, bunch_range):


def ifMakeFit(sat, func, *args, **kwargs):
    if not sat:
        return func(*args, **kwargs)
    else:
        return np.NaN


def plot_emit_bbb(df_ws, index_i="all", bunch_range="all"):

    if index_i == "all":
        index_i = [0, df_ws.shape[0]]

    if type(index_i) == int:
        plot_emit_1d(df_ws, index_i=index_i, bunch_range=bunch_range)
    elif index_i[1] - index_i[0] <= 3:
        plot_emit_1d(df_ws, index_i=index_i, bunch_range=bunch_range)
    else:
        plot_emit_3d(df_ws, index_i=index_i, bunch_range=bunch_range)


def getArrayCol(arr):
    return np.array(arr)[:, 1]


def ifGetSigma(df):
    if not df["saturated"]:
        return [np.array(df["fit"])[:, 1]]
    else:
        return np.NaN


def getSigmaFull(x):
    try:
        return np.abs(x[1])
    except TypeError:
        return np.NaN


def makeSigmaMean(fitOpt):
    return np.mean(np.array(fitOpt)[:, 1])


def get_rms_hist(x, y):
    y_new = y - np.min(y)
    cent = y_new.dot(x) / y_new.sum()
    rms = np.sqrt(y_new.dot((x - cent) ** 2) / y_new.sum())
    return rms


def get_mean_hist(x, y):
    y_new = y - np.min(y)
    cent = y_new.dot(x) / y_new.sum()
    return cent


def from_hdf5_to_dict(data):
    new_dic = {}
    for key in data.keys():
        new_dic[key] = np.array(data[key])
    return new_dic


def crop_profiles(data, lims):
    new_data = {}
    x1 = lims[0]
    x2 = lims[1]
    y1 = lims[2]
    y2 = lims[3]
    xmask = (data["xAxis"] > x1) & (data["xAxis"] < x2)
    ymask = (data["yAxis"] > y1) & (data["yAxis"] < y2)
    new_data["x_ax"] = data["xAxis"][xmask]
    new_data["x_profile"] = np.mean(data["xProfiles"], axis=0)[xmask]
    new_data["y_ax"] = data["yAxis"][ymask]
    new_data["y_profile"] = np.mean(data["yProfiles"], axis=0)[ymask]

    return new_data


def calculateRMS(x, signal, with_test=False):
    signal /= max(signal)
    max_index, max_value = max(enumerate(signal), key=operator.itemgetter(1))
    leftsignal = signal[0:max_index]
    rightsignal = signal[max_index:]

    leftMin = np.array(leftsignal)
    rightMin = np.array(rightsignal)

    findLMin = argrelextrema(leftMin, np.less)[0][-1]
    findRMin = argrelextrema(rightMin, np.less)[0][0] + len(leftsignal)

    cropped_x, cropped_y = x[findLMin:findRMin], signal[findLMin:findRMin]
    rms = get_rms_hist(cropped_x, cropped_y)

    print("RMS = ", rms)

    if with_test:
        plt.plot(x[0:findLMin], signal[0:findLMin], "b")
        plt.plot(cropped_x, cropped_y, "r")
        plt.plot(x[findRMin:], signal[findRMin:], "b")

    return rms, cropped_x, cropped_y


def SNR_single(x, signal, with_test):
    signal /= max(signal)
    max_index, max_value = max(enumerate(signal), key=operator.itemgetter(1))
    leftsignal = signal[0:max_index]
    rightsignal = signal[max_index:]

    leftMin = np.array(leftsignal)
    rightMin = np.array(rightsignal)

    findLMin = argrelextrema(leftMin, np.less)[0][-1]
    findRMin = argrelextrema(rightMin, np.less)[0][0] + len(leftsignal)

    Anoise = np.mean(list(signal[0:findLMin]) + list(signal[findRMin:]))
    # Asignal = 1-(signal[findLMin]+signal[findRMin])/2
    Asignal = 1 - Anoise

    #     snr_value = 20*np.log10(Asignal/Anoise);
    snr_value = Asignal / Anoise

    if with_test:
        plt.plot(x[0:findLMin], signal[0:findLMin], "b")
        plt.plot(x[findLMin:findRMin], signal[findLMin:findRMin], "r")
        plt.plot(x[findRMin:], signal[findRMin:], "b")
        plt.plot([x[max_index], x[max_index]], [1, 1 - Asignal], "r--")
        plt.plot(x, np.zeros(len(x)) + Anoise, "b--")

        print(snr_value)
    return snr_value


def SNR(x, signal, with_test=False, axis=0):
    signal = np.array(signal)
    if len(signal.shape) == 1:
        return SNR_single(x, signal, with_test)
    else:
        tot_snr = []
        for i in range(signal.shape[axis]):
            tot_snr.append(SNR_single(x, signal[i], with_test=with_test))
        return tot_snr


def get_sps_ws(plane, t1, t2, cut=10, fundamental=""):

    if plane == "h":
        bws = [
            "SPS.BWS.51995.H_ROT:PROJ_BUNCH_DATASET1",
            "SPS.BWS.51995.H_ROT:PROF_POSITION_IN",
            "SPS.BWS.51995.H_ROT:NB_PTS_SCAN_IN",
            "SPS.BWS.51995.H_ROT.APP.IN:ENERGY",
            "SPS.BWS.51995.H_ROT.APP.IN:BETA",
        ]

        # if beta == '':
        # beta = 86.8
    else:
        bws = [
            "SPS.BWS.41677.V_ROT:PROJ_BUNCH_DATASET1",
            "SPS.BWS.41677.V_ROT:PROF_POSITION_IN",
            "SPS.BWS.41677.V_ROT:NB_PTS_SCAN_IN",
            "SPS.BWS.41677.V_ROT.APP.IN:ENERGY",
            "SPS.BWS.41677.V_ROT.APP.IN:BETA",
        ]
        # if beta == '':
        # beta = 71.02

    if fundamental != "":
        df_ws = fromTimberToDataFrame(bws, t1=t1, t2=t2, fundamental=fundamental)
    else:
        df_ws = fromTimberToDataFrame(bws, t1=t1, t2=t2)

    df_ws.columns = ["profile", "x_axis", "beta", "momentum", "np", "cycleStamp"]

    df_ws["plane"] = plane

    df_ws["len_profile"] = df_ws["profile"].apply(len)

    df_ws["profile_bbb"] = df_ws.apply(
        lambda x: change_shape(x["profile"], x["len_profile"], x["np"]), axis=1
    )

    df_ws["profile_bbb"] = df_ws["profile_bbb"].apply(lambda x: x[0])

    df_ws["x_axis"] = df_ws["x_axis"].apply(lambda x: np.array(x) / 1e3)

    df_ws["num_bunches"] = df_ws["profile_bbb"].apply(lambda x: len(x))

    df_ws["saturated"] = df_ws["profile_bbb"].apply(lambda x: check_saturation(x))

    df_ws["fit"] = df_ws.apply(
        lambda x: ifMakeFit(
            x["saturated"], make_fit, x["x_axis"], x["profile_bbb"], cut=cut
        ),
        axis=1,
    )

    df_ws["snr"] = df_ws.apply(lambda x: SNR(x["x_axis"], x["profile_bbb"]), axis=1)

    df_ws["full_beam"] = df_ws["profile_bbb"].apply(lambda x: np.sum(x, axis=0))

    df_ws["sigma_mean"] = df_ws.apply(
        lambda x: ifMakeFit(x["saturated"], makeSigmaMean, x["fit"]), axis=1
    )

    # df_ws['sigma_bbb'] = df_ws['fit'].apply(lambda x: np.array(x)[:, 1])
    df_ws["sigma_bbb"] = df_ws.apply(lambda x: ifGetSigma(x), axis=1)

    df_ws["emit_bbb"] = df_ws.apply(
        lambda x: ifMakeFit(
            x["saturated"],
            emittance_from_sigma,
            x["sigma_bbb"],
            x["momentum"],
            x["beta"],
        ),
        axis=1,
    )

    # df_ws['full_beam_fit'] = df_ws.apply(lambda x: make_fit_full(x['x_axis'], x['full_beam'], cut=cut), axis=1)
    df_ws["full_beam_fit"] = df_ws.apply(
        lambda x: ifMakeFit(
            x["saturated"], make_fit_full, x["x_axis"], x["full_beam"], cut=cut
        ),
        axis=1,
    )

    # df_ws['full_beam_fit'] = df_ws['full_beam_fit'].apply(lambda x: x[0])

    df_ws["sigma_tot"] = df_ws["full_beam_fit"].apply(lambda x: getSigmaFull(x))

    df_ws["norm_emittance"] = df_ws.apply(
        lambda x: np.array(
            emittance_from_sigma(x["sigma_tot"], x["momentum"], x["beta"])
        )[0],
        axis=1,
    )

    df_ws["snr_tot"] = df_ws.apply(
        lambda x: (x["saturated"], SNR, x["x_axis"], x["full_beam"]), axis=1
    )

    return df_ws


def convertRadialTrim(optics, momentum, trim):
    alpha_dic = {"Q20": 0.003103331197, "Q26": 0.001927947581, "FT": 0.001853810952}

    alpha_c = alpha_dic[optics]
    delta_f_0 = -214.7
    f_0 = 200394400.0

    gamma = np.sqrt(momentum ** 2 + 0.938 ** 2) / 0.938

    eta = alpha_c - (1 / gamma ** 2)

    delta_f = trim * delta_f_0

    delta_p = -(delta_f / f_0) / eta

    return delta_p * 1e3


def sortList(x, y):
    if isinstance(y[0], tuple) or isinstance(y[0], list):
        return zip(*sorted(zip(x, *(y))))
    else:
        return zip(*sorted(zip(x, y)))


def fromXY2data(obs_x, obs_y, size=1000):

    # Center the x data in zero and normalized the y data to the area of the curve
    n_x = obs_x - obs_x[np.argmax(obs_y)]
    obs_y = obs_y - min(obs_y)
    n_y = obs_y / sum(obs_y)
    dx = np.abs(n_x[2] - n_x[1])
    # Generate a distribution of points matcthing the curve
    data = np.random.choice(a=n_x, size=size, p=n_y)
    lines = (n_x, n_y)
    return data, lines, dx


class BTV(object):
    def __init__(self, data):

        self.data = data or {}

        if data != {} and data != None:
            for key in data:
                setattr(self, key, np.array(data[key]))


def mixGaussFit(data, x_ax, n_gauss):
    number_points = len(data)

    gmm = GaussianMixture(n_components=n_gauss, covariance_type="spherical")
    gmm.fit(np.reshape(data, (number_points, 1)))
    gauss_mixt = np.array(
        [
            p * norm.pdf(x_ax, mu, sd)
            for mu, sd, p in zip(
                gmm.means_.flatten(), np.sqrt(gmm.covariances_.flatten()), gmm.weights_
            )
        ]
    )
    gauss_mixt_t = np.sum(gauss_mixt, axis=0)
    mu = gmm.means_.flatten()
    sigma = np.sqrt(gmm.covariances_.flatten())

    return gauss_mixt_t, gauss_mixt, mu, sigma


def getBTVdataFromLogging(btv_name, t1, t2):
    btv = [
        btv_name + "/Image#imageSet",
        btv_name + "/Image#imagePositionSet1",
        btv_name + "/Image#imagePositionSet2",
        btv_name + "/Image#saturationThreshold",
    ]

    data = fromTimberToDataFrame([btv[0]], t1, t2)
    for btv_var in btv[1:]:
        data_new = fromTimberToDataFrame([btv_var], t1, t2)
        data = pd.concat([data, data_new], axis=1, sort=False)
    if len(data) == 0:
        print("Warning: no data in the requested time interval")
        return None

    data["bins_x"] = data[btv[1]].apply(lambda x: np.array(x).flatten())
    data["bins_y"] = data[btv[2]].apply(lambda x: np.array(x).flatten())

    data["len_y"] = data["bins_y"].apply(lambda x: len(x))
    data["len_x"] = data["bins_x"].apply(lambda x: len(x))

    data["image"] = data.apply(
        lambda x: [np.array(x[btv[0]][0]).reshape(x["len_y"], x["len_x"])], axis=1
    )
    # # Check saturation
    # data['sat'] = data.apply(lambda x: x['image'][0][x['image'][0] == x[btv[3]]], axis=1)

    data["x_hist"] = data["image"].apply(lambda x: np.sum(x[0], axis=0))
    data["y_hist"] = data["image"].apply(lambda x: np.sum(x[0], axis=1))

    return data


def plotBTVDdata(df, row):
    X, Y = np.meshgrid(df["bins_x"].iloc[row], df["bins_y"].iloc[row])
    plt.contourf(X, Y, df["image"].iloc[row][0])


def plotFitBTV1Ddata(df, plane="x", plot=True):

    x, y = df["bins_" + plane], df[plane + "_hist"]
    data, lines, dx = fromXY2data(x, y, size=10000)
    # Plot the data
    if plot:
        plt.figure()
        plt.hist(data, normed=True, label="Normalized observed flux")
        plt.plot(lines[0], lines[1] / dx)
        plt.legend()

    n_gauss = 5
    fit_tot, fit_part, mus, sigmas = mixGaussFit(data, lines[0], n_gauss)
    if plot:
        plt.figure()
        plt.plot(lines[0], lines[1] / dx)
        plt.plot(lines[0], fit_tot, label=str(n_gauss) + " components fit")

        for i in range(len(fit_part)):
            plt.plot(lines[0], fit_part[i], label=r"$\sigma$: %.2f" % sigmas[i])

    plt.legend()


def str2datetime(string, formatDate="%d%b%y_%H-%M-%S"):
    """
    Input string example from AutoQ measurements: 21Aug17_10-34-22
    %d%b%y_%H-%M-%S
    """
    try:
        return datetime.strptime(string, formatDate)
    except ValueError:
        print("Time should be in the format: " + formatDate)
        raise


def get_bs_temp(data, bqm_bucket_var, min_bucket):
    bs_temp = []
    d = data[bqm_bucket_var]
    if data["n_bunches"] > 1:
        for i in range(len(d) - 1):
            if d[i + 1] > 0:
                bs = (d[i + 1] - d[i]) * min_bucket
                bs_temp.append(bs)
        return [bs_temp]
    else:
        return [[0]]


def get_batch_spacing(data):
    if len(data) > 0:
        return min(data)
    else:
        return 0


def getTrainInfo(t1, t2, machine="sps"):

    if machine == "sps":
        bqm_bucket_var = "SPS.BQM:FILLED_BUCKETS"
        int_ft = "SPSQC:INTENSITY_FLAT_TOP"
        int_pb = "SPS.BQM:BUNCH_INTENSITIES"
        df_train = fromTimberToDataFrame(
            [bqm_bucket_var, int_ft, int_pb],
            t1,
            t2,
            master=bqm_bucket_var,
            fundamental="%SPS%LHC%",
        )
        min_bucket = 5.0
        min_batch = 150.0

    else:
        bqm_bucket_var = "LHC.BQM.B1:FILLED_BUCKETS"
        int_pb_b1 = "LHC.BCTFR.A6R4.B1:BUNCH_INTENSITY"
        # int_pb_b2 = 'LHC.BCTFR.A6R4.B2:BUNCH_INTENSITY'
        inst_lumi_lhcb = "LHCB:FILL_LUMI_PEAK"
        df_train = fromTimberToDataFrame(
            [bqm_bucket_var, inst_lumi_lhcb], t1, t2, master=bqm_bucket_var
        )
        min_bucket = 2.5
        min_batch = 700.0

    df_train["n_bunches"] = df_train[bqm_bucket_var].apply(
        lambda x: len(np.array(x)[np.array(x) > 0.0])
    )
    df_train = df_train[df_train["n_bunches"] > 0]
    df_train["bs_temp"] = df_train.apply(
        lambda x: get_bs_temp(x, bqm_bucket_var, min_bucket), axis=1
    )
    df_train["bs_temp"] = df_train["bs_temp"].apply(lambda x: x[0])
    df_train["bs"] = df_train["bs_temp"].apply(lambda x: min(x))
    df_train["b_ss"] = df_train["bs_temp"].apply(
        lambda x: np.array(x)[np.array(x) > min_batch]
    )
    df_train["n_batches"] = df_train["b_ss"].apply(lambda x: len(x) + 1)
    df_train["bpb"] = df_train.apply(lambda x: x["n_bunches"] / x["n_batches"], axis=1)
    df_train["batch_spacing"] = df_train.apply(
        lambda x: get_batch_spacing(x["b_ss"]), axis=1
    )

    return df_train


def loadmat(filename):
    """
    this function should be called instead of direct spio.loadmat
    as it cures the problem of not properly recovering python dictionaries
    from mat files. It calls the function check keys to cure all entries
    which are still mat-objects
    """
    data = spio.loadmat(filename, struct_as_record=False, squeeze_me=True)
    return _check_keys(data)


def _check_keys(dict):
    """
    checks if entries in dictionary are mat-objects. If yes
    todict is called to change them to nested dictionaries
    """
    for key in dict:
        if isinstance(dict[key], spio.matlab.mio5_params.mat_struct):
            dict[key] = _todict(dict[key])
    return dict


def _todict(matobj):
    """
    A recursive function which constructs from matobjects nested dictionaries
    """
    dict = {}
    for strg in matobj._fieldnames:
        elem = matobj.__dict__[strg]
        if isinstance(elem, spio.matlab.mio5_params.mat_struct):
            dict[strg] = _todict(elem)
        else:
            dict[strg] = elem
    return dict


def get_first_last(bucket_pos, bucket_size, batch_spacing_limits):
    """
    Function to obtain the last and first bunch in a train
    """
    index_first = []
    index_last = []
    bs = []
    for i in range(1, len(bucket_pos)):
        delta = (bucket_pos[i] - bucket_pos[i - 1]) * bucket_size
        if batch_spacing_limits[1] > delta and delta > batch_spacing_limits[0]:
            index_last.append(i - 1)
            index_first.append(i)
            bs.append(delta)
    indexes = (index_first, index_last)
    return indexes, bs


def xlabel(text, **kwargs):
    plt.xlabel(text, horizontalalignment="right", x=1.0, **kwargs)


def ylabel(text, **kwargs):
    plt.ylabel(text, horizontalalignment="right", y=1.0, **kwargs)


def bin_ndarray(ndarray, new_shape, operation="sum"):
    """
    Bins an ndarray in all axes based on the target shape, by summing or
        averaging.

    Number of output dimensions must match number of input dimensions and
        new axes must divide old ones.

    Example
    -------
    >>> m = np.arange(0,100,1).reshape((10,10))
    >>> n = bin_ndarray(m, new_shape=(5,5), operation='sum')
    >>> print(n)

    [[ 22  30  38  46  54]
     [102 110 118 126 134]
     [182 190 198 206 214]
     [262 270 278 286 294]
     [342 350 358 366 374]]

    """
    operation = operation.lower()
    if not operation in ["sum", "mean"]:
        raise ValueError("Operation not supported.")
    if ndarray.ndim != len(new_shape):
        raise ValueError("Shape mismatch: {} -> {}".format(ndarray.shape, new_shape))
    compression_pairs = [(d, c // d) for d, c in zip(new_shape, ndarray.shape)]
    flattened = [l for p in compression_pairs for l in p]
    ndarray = ndarray.reshape(flattened)
    for i in range(len(new_shape)):
        op = getattr(ndarray, operation)
        ndarray = op(-1 * (i + 1))
    return ndarray


def make_fft(ft, t):
    """
    Make FFT and return array of module and axis of frequencies
    :param ft: time domain signal
    :param t: time axis
    :return: frequency axis, frequency domain frequency (module)
    """
    len_fft = int(len(ft) / 2)
    Y = np.fft.fft(ft)[1:len_fft]
    freq = np.fft.fftfreq(len(ft), t[1] - t[0])[1:len_fft]
    return freq, Y


def make_fft_comparison(alim, data, times, label):

    """
    @ alim: is a list of two strings, first is the measured current, second is the reference, third is the name
    """
    plt.figure(figsize=(6, 2))

    plt.title(alim[2] + "   " + label)

    delta_1 = np.array(data["2017"][alim[0]].iloc[0]) - np.array(
        data["2017"][alim[1]].iloc[0]
    )
    delta_2 = np.array(data["2018"][alim[0]].iloc[0]) - np.array(
        data["2018"][alim[1]].iloc[0]
    )

    t = np.arange(times[0], times[1]) * 1e-3  # in seconds
    current_1 = delta_1[times[0] : times[1]]
    current_2 = delta_2[times[0] : times[1]]

    f1, fft1 = make_fft(current_1, t)
    f2, fft2 = make_fft(current_2, t)

    plt.plot(f1, np.abs(fft1), label="2017", lw=3)
    plt.plot(f2, np.abs(fft2), label="2018")

    plt.ylabel(r"|$\mathcal{F}(I(t))$| / $A s$")
    plt.xlabel("freq / Hz")
    plt.yscale("log")
    # plt.xscale('log')

    plt.legend()


def make_current_comparison(alim, data):

    """
    @ alim: is a list of two strings, first is the measured current, second is the reference, third is the name
    """
    plt.figure(figsize=(6, 2))

    plt.title(alim[2])

    plt.plot(
        np.array(data["2017"][alim[0]].iloc[0])
        - np.array(data["2017"][alim[1]].iloc[0]),
        label="2017",
    )
    plt.plot(
        np.array(data["2018"][alim[0]].iloc[0])
        - np.array(data["2018"][alim[1]].iloc[0]),
        label="2018",
    )

    plt.ylabel(r"$\Delta I$ / A")
    plt.xlabel("time / ms")
    plt.ylim(-50e-3, 50e-3)

    plt.legend()
    plt.twinx()

    plt.plot(data["2017"][alim[1]].iloc[0], label="2017", color="k")
    plt.ylabel(r"$I_{ref}$ / A")


def makeBPMlistFile(file_name, plane, output_file):

    raw_data_h = []
    raw_data_v = []

    with open(file_name, "r") as f:
        for line in f:
            if "BP" in line and "H" in line:

                raw_data_h.append(re.search(r"BP(.+?)H[.]Beam", line).group())
            elif "BP" in line and "V" in line:
                raw_data_v.append(re.search(r"BP(.+?)Beam", line).group())

    if plane == "H":
        raw_data_h.sort(key=lambda x: x.split(".")[1])
        with open(output_file, "wb") as myfile:
            wr = csv.writer(myfile, delimiter="\n", quoting=csv.QUOTE_ALL)
            wr.writerow(raw_data_h)
    if plane == "V":
        raw_data_v.sort(key=lambda x: x.split(".")[1])
        with open(output_file, "wb") as myfile:
            wr = csv.writer(myfile, delimiter="\n", quoting=csv.QUOTE_ALL)
            wr.writerow(raw_data_v)


def makeBPMlist(file_name, plane):

    raw_data_h = []
    raw_data_v = []

    with open(file_name, "r") as f:
        for line in f:
            if "BP" in line and "H" in line:

                raw_data_h.append(re.search(r"BP(.+?)H[.]Beam", line).group())
            elif "BP" in line and "V" in line:
                raw_data_v.append(re.search(r"BP(.+?)Beam", line).group())

    if plane == "H":
        raw_data_h.sort(key=lambda x: x.split(".")[1])
        return raw_data_h

    if plane == "V":
        raw_data_v.sort(key=lambda x: x.split(".")[1])
        return raw_data_v


def getAlohaOutput(file_name, file_name_bpms, plane):

    data = np.genfromtxt(
        file_name, delimiter=",", names=True, skip_header=1, dtype="float32"
    )

    mean = "Y"
    err_p = "Y_1"
    err_m = "Y_2"

    bpms = makeBPMlist(file_name_bpms, plane)

    bpms = [ele.strip('""')[:-7] for ele in bpms]

    if plane == "H":
        data = data[: len(bpms)][[mean, err_m, err_p]]
    if plane == "V":
        data = data[-len(bpms) :][[mean, err_m, err_p]]

    dd_data = pd.DataFrame(data, index=bpms)

    dd_data.columns = ["mean", "err_m", "err_p"]

    return dd_data


def dict_fromkeys(list_ini, array_ini):
    return {key: array_ini.copy() for key in list_ini}


def plot_sc(df, cycle_offset=0, cycles=4, **plot_kwds):
    """
    The first column should be the current that will represent the cycles
    """
    start = df.index[cycle_offset]
    main_i = df.columns[0]
    for i in range(0, cycles):
        t = np.linspace(
            1,
            len(df[main_i].iloc[cycle_offset + i]),
            len(df[main_i].iloc[cycle_offset + i]),
        )
        time_delta = df.index[cycle_offset + i] - start
        t0 = time_delta.total_seconds() * 1e3
        t_cycle = t0 + t
        plt.plot(t_cycle * 1e-3, df[main_i].iloc[cycle_offset + i] * 1e-3, **plot_kwds)


def plot_sc_inTime(df, cycle_offset=0, cycles=4, **plot_kwds):
    """
    The first column should be the current that will represent the cycles
    """
    main_i = df.columns[0]
    for i in range(0, cycles):
        start = df.index[cycle_offset + i]
        samples = len(df[main_i].iloc[cycle_offset + i])
        t0 = pd.Timestamp(start)
        t_cycle = pd.Timestamp(t0 + datetime.timedelta(seconds=samples * 1e-3))
        t = np.linspace(t0.value, t_cycle.value, samples)
        t = pd.to_datetime(t)
        plt.plot(t, df[main_i].iloc[cycle_offset + i] * 1e-3, **plot_kwds)


def append(a, b):
    return np.append(a, [b], axis=0)


def coll_aperture(file_test, search_exp, n_sig, h_or_v, element, sigma, *args):
    """

    @param file_test: file containing collimators apertures
    @param search_exp: collimator exact name in capital letters (ex: 'TCDIH.20607')
    @param n_sig: number of betatron sigmas at which the jaws have to be (ex: '5')
    @param h_or_v: horizontal or vertical depending on the nature of the collimator ('h', 'v')
    @param element: list containing all elements into the line (ex. from twiss column "elements")
    @param sigma: list containing the betatron sigma values at each element
    @param args: [0] can be only 'nominal' if the original settings should be putted back on,
                 [1] can be 'h' or 'v' depending on the nature of the collimator
    """
    sig = sigma[element.index('"' + search_exp + '"') - 1]
    if not len(args) == 0:
        if args[1] == "v":
            n_h = 0.04000
            n_v = 0.01000
        elif args[1] == "h":
            n_h = 0.01000
            n_v = 0.04000
    for line in fileinput.input(file_test, inplace=1):
        if search_exp + "," in line:
            test = line.split()
            if test[2] != "at":
                if len(args) != 0 and args[0] == "nominal":
                    test[1] = "   " + test[1]
                    test[2] = "aperture={%.5f," % (n_h)
                    test[3] = "%.5f," % (n_v)
                    test[-1] += "\n"
                    line = " ".join(test)
                else:
                    if h_or_v == "h":
                        line = line.replace(test[2], "aperture={%.5f," % (n_sig * sig))
                    else:
                        test[1] = "   " + test[1]
                        test[3] = "%.5f," % (n_sig * sig)
                        test[-1] += "\n"
                        line = " ".join(test)
        elif search_exp + ":" in line and len(args) == 0:
            test = line.split()
            if test[1] == "marker,":
                if len(args) != 0 and args[0] == "nominal":
                    # test[1] = '   ' + test[1]
                    test[3] = "aperture={%.5f," % (n_h)
                    test[4] = "%.5f," % (n_v)
                    test[-1] += "\n"
                    line = " ".join(test)
                else:
                    if h_or_v == "h":
                        line = line.replace(test[3], "aperture={%.5f," % (n_sig * sig))
                    else:
                        test[1] = "      " + test[1]
                        test[4] = "%.5f," % (n_sig * sig)
                        test[-1] += "\n"
                        line = " ".join(test)
        elif search_exp + ":" in line and args[0] == "py":
            test = line.split()
            # test[1] = '   ' + test[1]
            test[3] = "aperture={%.5f," % (n_h)
            test[4] = "%.5f," % (n_v)
            test[-1] += "\n"
            line = " ".join(test)
        sys.stdout.write(line)


def coll_aperture_19(file_test, search_exp, n_sig, h_or_v, element, sigma, *args):
    """

    @param file_test: file containing collimators apertures for 1.9 m length collimators
    @param search_exp: collimator exact name in capital letters (ex: 'TCDIH.20607')
    @param n_sig: number of betatron sigmas at which the jaws have to be (ex: '5')
    @param h_or_v: horizontal or vertical depending on the nature of the collimator ('h', 'v')
    @param element: list containing all elements into the line (ex. from twiss column "elements")
    @param sigma: list containing the betatron sigma values at each element
    @param args: [0] can be only 'nominal' if the original settings should be putted back on,
                 [1] can be 'h' or 'v' depending on the nature of the collimator
    """
    sig = sigma[element.index('"' + search_exp + '"') - 1]
    if search_exp[-1] != "1":
        search_exp = search_exp[:-1] + "1"
    if not len(args) == 0:
        if args[1] == "v":
            n_h = 0.04000
            n_v = 0.01000
        elif args[1] == "h":
            n_h = 0.01000
            n_v = 0.04000
    for line in fileinput.input(file_test, inplace=1):
        if search_exp + "," in line:
            test = line.split()
            if test[2] != "at":
                if len(args) != 0 and args[0] == "nominal":
                    test[1] = "   " + test[1]
                    test[2] = "aperture={%.5f," % (n_h)
                    test[3] = "%.5f," % (n_v)
                    test[-1] += "\n"
                    line = " ".join(test)
                else:
                    if h_or_v == "h":
                        line = line.replace(test[2], "aperture={%.5f," % (n_sig * sig))
                    else:
                        test[1] = "   " + test[1]
                        test[3] = "%.5f," % (n_sig * sig)
                        test[-1] += "\n"
                        line = " ".join(test)
        elif search_exp + ":" in line and len(args) == 0:
            test = line.split()
            if test[1] == "marker,":
                if len(args) != 0 and args[0] == "nominal":
                    # test[1] = '   ' + test[1]
                    test[3] = "aperture={%.5f," % (n_h)
                    test[4] = "%.5f," % (n_v)
                    test[-1] += "\n"
                    line = " ".join(test)
                else:
                    if h_or_v == "h":
                        line = line.replace(test[3], "aperture={%.5f," % (n_sig * sig))
                    else:
                        test[1] = "      " + test[1]
                        test[4] = "%.5f," % (n_sig * sig)
                        test[-1] += "\n"
                        line = " ".join(test)
        elif search_exp + ":" in line and args[0] == "py":
            test = line.split()
            # test[1] = '   ' + test[1]
            test[3] = "aperture={%.5f," % (n_h)
            test[4] = "%.5f," % (n_v)
            test[-1] += "\n"
            line = " ".join(test)
        sys.stdout.write(line)


def fixlossfile(filename):
    linebuffer = ""
    for line in fileinput.input(filename, inplace=1):
        line = linebuffer + line
        breakdown = line.split('"')
        if len(breakdown) == 2:
            linebuffer = line[:-1]
            line = ""
        elif len(breakdown) >= 3:
            line = breakdown[0] + '"' + breakdown[1].split()[0] + '"\n'
        if len(line) > 0:
            sys.stdout.write(line)
            linebuffer = ""


def readtfs(filename, usecols=None, index_col=0, check_lossbug=True):
    header = {}
    nskip = 1
    closeit = False

    try:
        datafile = open(filename, "r")
        closeit = True
    except TypeError:
        datafile = filename

    for line in datafile:
        nskip += 1
        if line.startswith("@"):
            entry = line.strip().split()
            header[entry[1]] = eval(" ".join(entry[3:]))
        elif line.startswith("*"):
            colnames = line.strip().split()[1:]
            break

    if closeit:
        datafile.close()

    table = pd.read_csv(
        filename,
        delim_whitespace=True,
        skipinitialspace=True,
        skiprows=nskip,
        names=colnames,
        usecols=usecols,
        index_col=index_col,
    )

    if check_lossbug:
        try:
            table["ELEMENT"] = table["ELEMENT"].apply(lambda x: str(x).split()[0])
        except KeyError:
            pass
        try:
            for location in table["ELEMENT"].unique():
                if (
                    not location.replace(".", "")
                    .replace("_", "")
                    .replace("$", "")
                    .isalnum()
                ):
                    print(
                        "WARNING: some loss locations in "
                        + filename
                        + " don't reduce to alphanumeric values. For example "
                        + location
                    )
                    break
                if location == "nan":
                    print("WARNING: some loss locations in " + filename + " are 'nan'.")
                    break
        except KeyError:
            pass

    return header, table


def convString(list_ele):
    return [ele.decode("utf-8") for ele in list_ele]


def readTrimHistory(file_name):

    ts_index = []
    with open(file_name, "rb") as f:
        for line in f:
            #         line = f.readline()
            line = str(line, "utf-8")
            if line[0] == "T":
                line_list = line.split()
                date_temp = line_list[2][5:] + "_" + line_list[3][:-1]
                ts_index.append(str2datetime(date_temp, formatDate="%d-%m-%Y_%H:%M:%S"))

    output = pd.DataFrame(np.ones(len(ts_index)), index=ts_index)
    return output


def formDateToTimeStamp(datetime_obj):
    return time.mktime(datetime_obj.timetuple())


def redchisqg(ydata, ymod, deg=2, sd=None):
    """
    Returns the reduced chi-square error statistic for an arbitrary model,
    chisq/nu, where nu is the number of degrees of freedom. If individual
    standard deviations (array sd) are supplied, then the chi-square error
    statistic is computed as the sum of squared errors divided by the standard
    deviations. See http://en.wikipedia.org/wiki/Goodness_of_fit for reference.

    ydata,ymod,sd assumed to be Numpy arrays. deg integer.
    where
    ydata : data
    ymod : model evaluated at the same x points as ydata
    n : number of free parameters in the model
    sd : uncertainties in ydata

    Rodrigo Nemmen
    http://goo.gl/8S1Oo
    """
    # Chi-square statistic
    if sd == None:
        chisq = np.sum((ydata - ymod) ** 2)
    else:
        chisq = np.sum(((ydata - ymod) / sd) ** 2)

        # Number of degrees of freedom assuming 2 free parameters
    nu = ydata.size - 1 - deg

    return chisq / nu


def printProgressBar(
    iteration, total, prefix="", suffix="", decimals=1, length=100, fill="█"
):
    """
    Call in a loop to create terminal progress bar
    @params:
        iteration   - Required  : current iteration (Int)
        total       - Required  : total iterations (Int)
        prefix      - Optional  : prefix string (Str)
        suffix      - Optional  : suffix string (Str)
        decimals    - Optional  : positive number of decimals in percent complete (Int)
        length      - Optional  : character length of bar (Int)
        fill        - Optional  : bar fill character (Str)
    """
    percent = ("{0:." + str(decimals) + "f}").format(100 * (iteration / float(total)))
    filledLength = int(length * iteration // total)
    bar = fill * filledLength + "-" * (length - filledLength)
    print("\r%s |%s| %s%% %s" % (prefix, bar, percent, suffix), end="\r")
    # Print New Line on Complete
    if iteration == total:
        print()


def compareArrays(x1, y1, x2, y2, n=1000):
    x_min = max(min(x1), min(x2))
    x_max = min(max(x1), max(x2))
    x = np.linspace(x_min, x_max, n)
    y1_temp = np.interp(x, x1, y1)
    y2_temp = np.interp(x, x2, y2)
    return x, y1_temp, y2_temp


def str2num(string):
    # extract numbers from garbage string:
    s = string
    newstr = "".join((ch if ch in "0123456789.-e" else " ") for ch in s)
    return [float(i) for i in newstr.split()]


def tfsTableFromDF(data, file_name, title, includeIndex=True):
    date_now = datetime.now().strftime("%d/%m/%y %H.%M.%S")
    date_date = date_now.split()[0]
    date_time = date_now.split()[1]

    if includeIndex:
        if data.index.name is None:
            columns = ["N"] + list(data.columns)
        else:
            columns = [data.index.name] + list(data.columns)
    else:
        columns = list(data.columns)
    with open(file_name, "w") as fw:

        head = []
        head.append('@ TYPE          %04s "USER"\n')
        head.append(f'@ NAME         %{len(title):02d}s  "{title.upper()}"\n')
        head.append(f'@ DATE         %08s  "{date_date}"\n')
        head.append(f'@ TIME         %08s  "{date_time}"\n')

        tfs_tab = PrettyTable(["*"] + columns)
        tfs_tab.align = "r"
        tfs_tab.border = False
        tfs_tab.add_row(["$"] + ["%le"] * len(columns))

        with open(file_name, "w") as fp:
            for i in range(len(head)):
                fp.write(head[i])
            for i in range(len(data)):
                row = list(data.iloc[i])
                if "N" in columns:
                    row = [str(i)] + row

                tfs_tab.add_row([" "] + row)
            fp.write(tfs_tab.get_string())


def bokeh_figure(xlabel="", ylabel="", fig_size=(400, 300), title=""):
    f = b_fig(title=title, plot_width=fig_size[0], plot_height=fig_size[1])

    f.axis.major_label_text_font = "times"
    f.axis.axis_label_text_font = "times"
    f.axis.axis_label_text_font_style = "normal"
    f.axis.axis_label_text_font_size = "14pt"
    f.outline_line_color = "black"
    # f.sizing_mode = 'scale_width'

    f.xaxis.axis_label = xlabel
    f.yaxis.axis_label = ylabel
    f.xaxis.major_label_text_font_size = "12pt"
    f.yaxis.major_label_text_font_size = "12pt"

    return f
